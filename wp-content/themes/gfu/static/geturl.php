<?php
	/* Made by Hakan & Furkan */
	session_start();

	$links = array(
		'facebook' => 'http://www.facebook.com/GFU.net', 
		'google-plus' => 'https://plus.google.com/111387833094105129671',
		'xing' => 'http://www.xing.com/companies/gfucyrusag',
		'clients' => '/kundenliste.html',
        'ekomi' => 'https://www.ekomi.de/bewertungen-gfunet.html'
	);

	foreach($_POST as $key => $post) {
		if(array_key_exists($key, $links)) {
			header("location: $links[$key]");
			exit;
		}
	}
?>