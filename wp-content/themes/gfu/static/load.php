<?php
	# default redirect filename
	$filename = "404.html";
	
	# is browser a crawler
	if (isbot("") == false)
	{
		$key = array_keys($_POST);
		$filename = strtolower($key[0]) . ".html";
	}
	$key = array_keys($_POST);
    if($key[0] === 'blog') {
        header("Location: http://blog.gfu.net");
        exit(0);
    }
	$pos = strpos($key[0], '#');
	$pos2 = strpos($key[0], '?');
	if($pos)
	{
		$splittedParam = explode('#', $key[0]);
		$filename = $splittedParam[0].".html#".$splittedParam[1];
	}
	elseif($pos2) {			
		$splittedParam = explode('\?', $key[0]);
		$filename = $splittedParam[0].".html?".$splittedParam[1];
	}
	
	# and now redirect
	header("Location: $filename");

	######## Some usefull functions
	function get_url($base = true, $www = true, $query = true, $echo = false){
		$URL = ''; //open return variable
		$URL .= (($_SERVER['HTTPS'] != '') ? "https://" : "http://"); //get protocol
		$URL .= (($www == true && !preg_match("/^www\./", $_SERVER['HTTP_HOST'])) ? 'www.'.$_SERVER['HTTP_HOST'] : $_SERVER['HTTP_HOST']); //get host
		$path = (($_SERVER['REQUEST_URI'] != '') ? $_SERVER['REQUEST_URI'] : $_SERVER['PHP_SELF']); //tell the function what path variable to use
		$URL .= ((pathinfo($path, PATHINFO_DIRNAME) != '/') ? pathinfo($path, PATHINFO_DIRNAME).'/' : pathinfo($path, PATHINFO_DIRNAME)); //set up directory
		$URL .= (($base == true) ? pathinfo($path, PATHINFO_BASENAME) : ""); //add basename
		$URL  = preg_replace("/\?".preg_quote($_SERVER['QUERY_STRING'])."/", "", $URL); //remove query string if found in url
		$URL .= (($query == true && $_SERVER['QUERY_STRING'] != '') ? "?".$_SERVER['QUERY_STRING'] : ""); //add query string
		if($echo == true){
			echo $URL;
		}else{
			return $URL;
		}
	}

	function isbot($agent="") {
		//Handfull of Robots
		$bot_array      =array(
			"jeevesteoma",
			"msnbot",
			"slurp",
			"jeevestemoa",
			"gulper",
			"googlebot",
			"linkwalker",
			"validator",
			"webaltbot",
			"wget"
		);
		//no agent given => read from globals
		if ($agent=="")
			{
			@$agent=$_SERVER["HTTP_USER_AGENT"];
			}
		//replace all but alpha
		$agent=strtolower(preg_replace("/[^a-zA-Z _]*/","",$agent));
		//check f�r intersections
		return((BOOL)count(array_intersect(explode(" ",$agent),$bot_array)));
	}
	/*
	foreach ($_POST as $key => $value ) {
			echo $key;
		}
*/
?>