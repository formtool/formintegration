var paramsCookieName = 'booking';
var params = decodeCookie(getCookie(paramsCookieName));

function getCookie(name) {
	var pair = document.cookie.match(new RegExp(name + '=([^;]+)'));
	return !!pair ? pair[1] : null;
}

function decodeCookie(cookie) {
	if (cookie != null) {
		var decodedCookie = JSON.parse(decodeURIComponent(cookie.replace(/\+/g, '%20')));
		return decodedCookie;
	} else {
		return null;
	}
}

function insertParam(key, value) {
    key = encodeURI(key); value = encodeURI(value);

    var kvp = document.location.search.substr(1).split('&');

    var i=kvp.length; var x; while(i--) 
    {
        x = kvp[i].split('=');

        if (x[0]==key)
        {
            x[1] = value;
            kvp[i] = x.join('=');
            break;
        }
    }

    if(i<0) {kvp[kvp.length] = [key,value].join('=');}

    //this will reload the page, it's likely better to store this until finished
    document.location.search = kvp.join('&'); 
}

if (params['utm_expid'] && !location.search.indexOf("utm_expid")) {
	insertParam('utm_expid', params['utm_expid']);
	console.log('optimize param added to url');
}

jQuery( document ).ready( function( $ ) {
	$('#sidebar')
	.stick_in_parent({
		offset_top: $('.navbar').height()
	})
	.on('sticky_kit:bottom', function() {
		$(this).parent().css('position', 'static');
	})
	.on('sticky_kit:unbottom', function() {
		$(this).parent().css('position', 'relative');
	});

	// Recalc height of sticky sidebar if elements get collapsed
	$('#sidebar .panel-collapse, #rechnungsanschrift, #hotelreservierung').on('shown.bs.collapse hidden.bs.collapse', function() {
		$(document.body).trigger("sticky_kit:recalc");
	});

	if ($('input[name="rechnungsanschrift[]"]').prop("checked")) {
		$('#rechnungsanschrift').addClass('in');
	}
	if ($('input[name="hotelreservierung[]"]').prop("checked")) {
		$('#hotelreservierung').addClass('in');
	}
});