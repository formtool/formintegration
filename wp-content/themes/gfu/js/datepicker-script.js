$('.datepicker').daterangepicker({
  "singleDatePicker": true,
  "minDate": moment(),
  "locale": {
    "format": "DD.MM.YYYY",
    "applyLabel": "Bestätigen",
    "cancelLabel": "Abbrechen",
    "fromLabel": "Von",
    "toLabel": "Bis",
    "customRangeLabel": "Benutzerdefiniert",
    "daysOfWeek": [
      "So",
      "Mo",
      "Di",
      "Mi",
      "Do",
      "Fr",
      "Sa"
    ],
    "monthNames": [
      "Januar",
      "Februar",
      "März",
      "April",
      "Mai",
      "Juni",
      "Juli",
      "August",
      "September",
      "Oktober",
      "November",
      "Dezember"
    ]
  }
});