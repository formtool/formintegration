// blueimp javascript templates
!function(e){"use strict";var n=function(e,t){var r=/[^\w\-.:]/.test(e)?new Function(n.arg+",tmpl","var _e=tmpl.encode"+n.helper+",_s='"+e.replace(n.regexp,n.func)+"';return _s;"):n.cache[e]=n.cache[e]||n(n.load(e));return t?r(t,n):function(e){return r(e,n)}};n.cache={},n.load=function(e){return document.getElementById(e).innerHTML},n.regexp=/([\s'\\])(?!(?:[^{]|\{(?!%))*%\})|(?:\{%(=|#)([\s\S]+?)%\})|(\{%)|(%\})/g,n.func=function(e,n,t,r,c,u){return n?{"\n":"\\n","\r":"\\r","\t":"\\t"," ":" "}[n]||"\\"+n:t?"="===t?"'+_e("+r+")+'":"'+("+r+"==null?'':"+r+")+'":c?"';":u?"_s+='":void 0},n.encReg=/[<>&"'\x00]/g,n.encMap={"<":"&lt;",">":"&gt;","&":"&amp;",'"':"&quot;","'":"&#39;"},n.encode=function(e){return(null==e?"":""+e).replace(n.encReg,function(e){return n.encMap[e]||""})},n.arg="o",n.helper=",print=function(s,e){_s+=e?(s==null?'':s):_e(s);},include=function(s,d){_s+=tmpl(s,d);}","function"==typeof define&&define.amd?define(function(){return n}):"object"==typeof module&&module.exports?module.exports=n:e.tmpl=n}(this);

jQuery( document ).ready( function( $ ) {
  var cf7CookieName = 'cf7msm_posted_data';
  var paramsCookieName = 'booking';
  var cf7data = decodeCookie(getCookie(cf7CookieName));
  var params = decodeCookie(getCookie(paramsCookieName));

  function getCookie(name) {
    var pair = document.cookie.match(new RegExp(name + '=([^;]+)'));
    return !!pair ? pair[1] : null;
  }

  function updateCookie(cookieName, cookie, obj) {
    for (var key in obj) {
      cookie[key] = obj[key];
    }

    createCookie(cookieName, urlencode(JSON.stringify(cookie)), 0);
  }

  function decodeCookie(cookie) {
    if (cookie != null) {
      var decodedCookie = JSON.parse(decodeURIComponent(cookie.replace(/\+/g, '%20')));
      return decodedCookie;
    } else {
      return null;
    }
  }

  if (cf7data != null) {
    $('.form_reservierung input, .form_reservierung textarea, .form_reservierung select').each(function() {
      var $input = $(this);
      var inputName = $input.attr('name');

      $input.val(cf7data[inputName]);
    });
  }


  // Php like encoding
  function urlencode (str) {
    str = (str + '')
    return encodeURIComponent(str)
      .replace(/!/g, '%21')
      .replace(/'/g, '%27')
      .replace(/\(/g, '%28')
      .replace(/\)/g, '%29')
      .replace(/\*/g, '%2A')
      .replace(/%20/g, '+')
  }

  function createCookie(name, value, days) {
    var date = new Date();
    date.setTime(date.getTime() + (days * 24 * 60 * 60 * 1000));
    var expires = (days == 0) ? 0 : date.toUTCString();
    document.cookie = name + '=' + value + ';' +
                    'expires=' + expires + ';' +
                    'path=/;';
  }

  if (window.location.hash == '#vormerken') {
    $('#trigger-vormerken a').tab('show');
  }

  // actions based on params
  for (key in params) {
    var value = params[key];

    switch(key) {
      case 'hKnapp':
        if (value == 1) {
          $('#hKnappMsg').show();
        }
        break;
      case 'kDrei':
        if (value == 1) {
          $('.free-hint').hide();
        }
        break;
      case 'zerti':
        if (value != null) {
          $('#certBlock').show();
          $('.zpreis').text(value);
        }
        break;
      case 'T':
        var rawDate = (value.match('-')) ? value.split('-') : value;

        if (value.match('-')) {
          var startDate = [
            rawDate[0].split('.')[0],
            rawDate[0].split('.')[1],
            rawDate[0].split('.')[2]
          ];
          
          var endDate = [
            rawDate[1].split('.')[0],
            rawDate[1].split('.')[1],
            rawDate[1].split('.')[2]
          ];
          
          if (startDate[2] == "") {
            startDate[2] = endDate[2]
          }

          startDate = startDate.join('.');
          endDate = endDate.join('.');
        } else {
          startDate = rawDate;
          endDate = rawDate;
        }

        if ($('.datepicker').length > 0) {
          $('.datepicker[name="anreise"]').data('daterangepicker').setStartDate(startDate);
          $('.datepicker[name="abreise"]').data('daterangepicker').setStartDate(endDate);
        }

        break;
    }
  }
  
  $('#vormerkenBtn').click(function() {
    var obj = {};

    $('.form_reservierung input, .form_reservierung select, .form_reservierung textarea').each(function() {
      var name = $(this).attr('name');
      var value = $(this).val();
      obj[name] = value;
    });

    if (cf7data == null) {
      createCookie(cf7CookieName, urlencode(JSON.stringify(obj)), 0);
    } else {
      updateCookie(cf7CookieName, cf7data, obj);
    }
    createCookie('reserve', '1', 0);
  });

  // Add Participant Function
  function participants(trigger, container, prefix) {
    var addBtn = trigger;
    var removeBtn = '.removeParticipant';
    var item = '.participants__item';
    var namePattern = '^(' + prefix + ')(\\d*)(_)';
    var nameRegex = new RegExp(namePattern, 'g');
    var count = 0;
    var maxCount = 2;
    var kDrei = params['kDrei'];

    if ($(container).length == 0) return;

    function updateCf7Cookie(cookieName, obj) {
      var encodedObj;
      var $input;
      
      for (var key in obj) {
        if (key.match(nameRegex)) {
          $input = $('[name="' + key + '"]');
          if ($input.length == 0) {
            delete obj[key];
          }
        }
      }
      
      encodedObj = urlencode(JSON.stringify(obj));
      createCookie(cookieName, encodedObj, 0);
    }

    function checkPos(element, prefix) {
      var visibleElem = element + '.in';
      var $count;
      var $inputField;
      var $kDrei;
      var fieldName;
      var displayCount;
      
      function changeName(p1, p2, p3, offset) {
        return p2 + displayCount + offset;
      }
      
      $(visibleElem).each(function(i) {
        $count = $(this).find('.participants__count');
        $inputField = $(this).find('[name^="' + prefix + '"]');
        $kDrei = $(this).find('.participants__kdrei');
        displayCount = 2 + i;

        if (count == (maxCount - 1)) {
            $kDrei.remove();
        }
        
        $count.text(displayCount);
        
        $inputField.each(function() {
          fieldName = $(this).attr('name').replace(nameRegex, changeName);
          $(this).attr('name', fieldName);
        });
      });
    }

    function addParticipant() {
      var displayCount = 2 + count;
      var data = {
        "count": displayCount,
        "prefix": prefix + displayCount
      };
      
      if (count == maxCount) return;
      
      if (count == (maxCount - 1)) {
        $(addBtn).prop('disabled', true);
        
        if (kDrei == 0) {
          data['kDrei'] = kDrei;
        }
      }

      $(container).append(tmpl('participant', data));
      $(item + ':last-child').collapse('show');
      
      count++;
    }

    function restoreParticipant(obj, action) {
      var countHighest = 0;
      var formValues = {};
      
      for (var key in obj) {
        if (key.match(nameRegex)) {
          var count = key.match(/\d+/);
          countHighest = (countHighest < count) ? count : countHighest;

          formValues[key] = obj[key];
        }
      }
      
      for (i = 1; i < countHighest; i++) { 
          action();
      }

      for (var key in formValues) { 
          $('[name="' + key + '"]').val(formValues[key]);
      }
    }
    restoreParticipant(cf7data, addParticipant);

    $(addBtn).on('click', function() {
      addParticipant();
    });

    $(container).on('click', removeBtn, function() {
      if (count == maxCount) {
        $(addBtn).prop('disabled', false);
      }
          
      $(this)
        .parents(item)
        .collapse('hide')
        .on('hidden.bs.collapse', function() {
          $(this).remove();
          updateCf7Cookie(cf7CookieName, cf7data);
        });
      
        count--;
        checkPos(item, prefix);
    });
  }
  participants('#addParticipant', '#tnContainer', 'tn');
  participants('#addParticipantReserve', '#tnContainerReserve', 'tn');

  // Hotel Table
  $('.table_hotels__description').each(function() {
    var maxLength = 110;
    
    if ($(this).text().length > maxLength) {
      var excerpt = $(this).text().substring(0, maxLength);
      var remainingText = $(this).text().substring(maxLength);

      $(this).html(excerpt + '<span class="ellipsis">...</span><span class="remaining-text">' + remainingText + '</span>');
    }
  });

  $('.table_hotels__radio input[type=checkbox]').click(function() {
    $('.table_hotels__radio input[type=checkbox]').prop('checked', false);
    $(this).prop('checked', true);
  });


  $('.table_hotels__item').click(function() {
    $('.table_hotels__radio input[type=checkbox]').prop('checked', false);
    $(this).find('.table_hotels__radio input[type=checkbox]').prop('checked', true);
    $('.table_hotels .table_hotels__item').find('.table_hotels__description').removeClass('table_hotels__description--expanded');
    $(this).find('.table_hotels__description').addClass('table_hotels__description--expanded');
  });
  // End Hotel Table
});