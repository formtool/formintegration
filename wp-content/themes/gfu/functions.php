<?php

if ( ! class_exists( 'Timber' ) ) {
    add_action( 'admin_notices', function() {
        echo '<div class="error"><p>Timber not activated. Make sure you activate the plugin in <a href="' . esc_url( admin_url( 'plugins.php#timber' ) ) . '">' . esc_url( admin_url( 'plugins.php') ) . '</a></p></div>';
    });

    add_filter('template_include', function($template) {
        return get_stylesheet_directory() . '/static/no-timber.html';
    });

    return;
}

Timber::$dirname = array('templates', 'views');

class StarterSite extends TimberSite {

    function __construct() {
        add_theme_support( 'post-formats' );
        add_theme_support( 'post-thumbnails' );
        add_theme_support( 'menus' );
        add_theme_support( 'html5', array( 'comment-list', 'comment-form', 'search-form', 'gallery', 'caption' ) );
        add_filter( 'timber_context', array( $this, 'add_to_context' ) );
        add_filter( 'get_twig', array( $this, 'add_to_twig' ) );
        add_filter( 'wpcf7_form_elements', 'do_shortcode' ); // Activate Shortcode Execution for Contact Form 7
        add_action( 'init', array( $this, 'register_post_types' ) );
        add_action( 'init', array( $this, 'register_taxonomies' ) );
        add_action( 'wp_enqueue_scripts', array( $this, 'theme_enqueue_scripts' ) );
        add_action( 'wp_enqueue_scripts', array( $this, 'theme_enqueue_styles' ) );
        add_shortcode( 'formSteps', array( $this, 'formSteps_shortcode' ) );
        add_shortcode( 'condition', array( $this, 'condition_shortcode' ) );
       // add_shortcode( 'conditional', array( $this, 'conditional_shortcode' ) );

        parent::__construct();
    }

    function register_post_types() {
        //this is where you can register custom post types
    }

    function register_taxonomies() {
        //this is where you can register custom taxonomies
    }

    function theme_enqueue_scripts() {
        //this is where you can register your scripts
        wp_deregister_script( 'jquery' );

        wp_enqueue_script( 'jquery', get_template_directory_uri() . '/js/jquery-3.2.1.min.js', false, NULL, true );
        wp_enqueue_script( 'bootstrap', get_template_directory_uri() . '/js/bootstrap.min.js', array('jquery'), NULL, true );
        wp_enqueue_script( 'sticky-kit', get_template_directory_uri() . '/js/sticky-kit.min.js', array('jquery'), NULL, true );
        wp_enqueue_script( 'main', get_template_directory_uri() . '/js/main.js', array('jquery'), false, true );
        if (is_page_template('template-anmeldung.php') || is_page_template('template-supermail-abschluss.php') || is_page_template('template-vormerken.php')) {
            wp_enqueue_script( 'anmeldung-offen', get_template_directory_uri() . '/js/anmeldung-offen.js', array('jquery'), NULL, true );
            wp_enqueue_script( 'moment', get_template_directory_uri() . '/js/moment.js', false, NULL, true );
            wp_enqueue_script( 'daterangepicker', get_template_directory_uri() . '/js/daterangepicker.js', array('jquery'), false, true );
            wp_enqueue_script( 'datepicker-script', get_template_directory_uri() . '/js/datepicker-script.js', array('jquery'), false, true );
        }
    }

    function theme_enqueue_styles() {
        //this is where you can register your styles
        wp_enqueue_style( 'gfu', get_stylesheet_directory_uri() . '/css/gfu.css' );
        wp_enqueue_style( 'main', get_template_directory_uri() . '/css/main.css' );
        if (is_page_template('template-anmeldung.php') || is_page_template('template-supermail-abschluss.php') || is_page_template('template-vormerken.php')) {
            wp_enqueue_style( 'daterangepicker', get_template_directory_uri() . '/css/daterangepicker.css' );
        }
    }

    function condition_shortcode($atts, $content = null) {
        $atts = shortcode_atts( array(
            'isset' => 0
        ), $atts );

        if (isset($atts['isset']) || $atts['isset'] != 0) {
            return $content;
        } else {
            return;
        }
    }

    function formSteps_shortcode( $atts ) {
        $atts = shortcode_atts( array(
            'step' => 1
        ), $atts );
        $steps = array("Firma", "Teilnehmer", "Serviceleistungen", "Zusammenfassung");
        $stepLinks = array("/", "/teilnehmer", "/serviceleistungen", "/zusammenfassung");
        $stepsHtml = '';

        foreach($steps as $key => $step) {
            $count = $key + 1;
            $isCurrent = ( $count == $atts['step'] ) ? 'progress-steps__item--current' : '';
            $isActive = ( $count <= $atts['step'] ) ? 'progress-steps__item--active' : '';
            $stepLink = ( $count <= $atts['step'] ) ? '<a href="'.$stepLinks[$key].'" class="progress-steps__count">'.$count.'</a>' : '<span class="progress-steps__count">'.$count.'</span>';
            $stepsHtml .= '<li class="progress-steps__item '.$isActive.' '.$isCurrent.'">'.$stepLink.'<div class="progress-steps__label">'.$step.'</div> </li>';
        }

        return '<ol class="progress-steps">'.$stepsHtml.'</ol>';
    }

    function add_to_context( $context ) {
        $context['foo'] = 'bar';
        $context['stuff'] = 'I am a value set in your functions.php file';
        $context['notes'] = 'These values are available everytime you call Timber::get_context();';
        $context['menu'] = new TimberMenu();
        $context['site'] = $this;
        return $context;
    }

    function myfoo( $text ) {
        $text .= ' bar!';
        return $text;
    }

    function add_to_twig( $twig ) {
        /* this is where you can add your own functions to twig */
        $twig->addExtension( new Twig_Extension_StringLoader() );
        $twig->addFilter('myfoo', new Twig_SimpleFilter('myfoo', array($this, 'myfoo')));
        return $twig;
    }

}

new StarterSite();