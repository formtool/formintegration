<?php
/**
 * Template Name: Template Supermail (Zusammenfassung)
 *
 */

if (isset($_COOKIE['booking-finalized'])) {
    header("location: https://www.gfu.net/bereitsgebucht.html");
}

get_header();
?>

<div class="sem-overview">
    <div class="sem-overview__title">
        <span class="sem-overview__label">Seminar</span>
        <span class="semtitel"></span><?php echo do_shortcode('[Param params="S"]'); ?> (<span
            class="semid"><?php echo do_shortcode('[Param params="I"]'); ?></span>)
    </div>

    <div class="sem-overview__row">
        <div class="sem-overview__cell">
            <span class="sem-overview__label">Termin</span>
            <span class="semtermin"><?php echo do_shortcode('[Param params="T"]'); ?>Test</span>
        </div>

        <div class="sem-overview__cell">
            <span class="sem-overview__label">Ort</span>
            <span class="semort"><?php echo do_shortcode('[Param params="ort"]'); ?></span>
        </div>

        <div class="sem-overview__cell">
            <span class="sem-overview__label pLabel">Preis</span>
            <span class="price">
		<span class="sempreis p"><?php echo do_shortcode('[Param params="G"]'); ?></span> &euro;
                <span class="asterisk"></span>
	</span>
            <span class="sem-overview__mwst">(zzgl. Mwst.)</span>
        </div>
    </div>
</div>
<div class="price-included">
    <span class="asterisk"></span> <strong>Im Preis enthalten:</strong>
    <div class="row">
        <div class="col-sm-6">
            <ul class="list">
                <li>Voll ausgestatteter Arbeitsplatz pro Teilnehmer</li>
                <li>Fachbuch zum Seminar</li>
                <li>Teilnahmezertifikat</li>
                <li>Kostenloser persönlicher Parkplatz</li>
            </ul>
        </div>
        <div class="col-sm-6">
            <ul class="list">
                <li>Kostenloser Shuttle-Service</li>
                <li>Frühstück, Snacks und Getränke ganztägig</li>
                <li>Mittagessen im eigenen Restaurant, täglich 6 Menüs, auch vegetarisch</li>
            </ul>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-sm-6">
        <h4>Buchen</h4>
        <p>Buchen Sie jetzt! Das Seminar findet garantiert statt. Die Buchung kann bis zum letzten Arbeitstag vor der
            Schulung kostenlos storniert werden.<br>
            Sie können auch per <a href="#" id="get-print">Faxformular</a> oder Telefon buchen.
        </p>
    </div>
    <div class="col-sm-6">
        <h4>Unverbindlich vormerken</h4>
        <p>Sichern Sie sich Ihren Seminarplatz schon vor der Buchung! So können Sie in Ruhe alle Modalitäten regeln. Das
            Vormerken des Kurses ist für Sie nicht verbindlich. Die Buchung sollte aber innerhalb von 10 Tagen erfolgen,
            da wir Ihren Platz danach wieder freigeben müssen.</p>
    </div>
</div>

<ul class="nav nav-tabs nav-anmeldung">
    <li class="active" id="trigger-buchen">
        <a href="#buchen" aria-expanded="true" data-toggle="tab">Buchen</a>
    </li>
    <li id="trigger-vormerken">
        <a href="#vormerken" aria-expanded="false" data-toggle="tab">Unverbindlich vormerken</a>
    </li>
</ul>
<br>
<div class="tab-content form_anmeldung anmeldung-offen form-horizontal">
    <div class="tab-pane active" id="buchen">
        <div class="form form--multistep">
            <?php
                $tnHandy = do_shortcode('[Writevalues cookievalue="Handy_Nr"]');
                $tn2Handy = do_shortcode('[Writevalues cookievalue="tn2_Handy_Nr"]');
                $tn3Handy = do_shortcode('[Writevalues cookievalue="tn2_Handy_Nr"]');
                $tn2 = do_shortcode('[Writevalues cookievalue="tn2_vorname"]');
                $tn3 = do_shortcode('[Writevalues cookievalue="tn3_vorname"]');
                $rechnung = do_shortcode('[Writevalues cookievalue="altbill"]');
                $zertifizierung = do_shortcode('[Writevalues cookievalue="zertifikat"]');
                $hotelreservierung = do_shortcode('[Writevalues cookievalue="hotelreservierung"]');
                $parkplatz = do_shortcode('[Writevalues cookievalue="parkplatz"]');
                $shuttle = do_shortcode('[Writevalues cookievalue="shuttle"]');
                $hotel = do_shortcode('[Writevalues cookievalue="hotel"]');
                $kommentar = do_shortcode('[Writevalues cookievalue="comments"]');
                echo do_shortcode('[formSteps step="4"]');
            ?>

            <fieldset class="well">
                <legend>
                    Firma
                    <a href="/" class="btn btn-default btn-xs pull-right">Ändern</a>
                </legend>
                <div class="row">
                    <div class="col-sm-3"><strong>Firma:</strong></div>
                    <div class="col-sm-8"><?php echo do_shortcode('[Writevalues cookievalue="firm"]')?></div>
                </div>
                <div class="row">
                    <div class="col-sm-3"><strong>Straße:</strong></div>
                    <div class="col-sm-8"><?php echo do_shortcode('[Writevalues cookievalue="str"]')?></div>
                </div>
                <div class="row">
                    <div class="col-sm-3"><strong>PLZ/Ort:</strong></div>
                    <div class="col-sm-8"><?php echo do_shortcode('[Writevalues cookievalue="plz"]')?> <?php echo do_shortcode('[Writevalues cookievalue="ort"]')?></div>
                </div>

                <?php if (!empty($rechnung)) { ?>
                <br>
                <fieldset class="well well-inner">
                    <legend>Abweichende Rechnungsanschrift</legend>
                    <div class="row">
                        <div class="col-sm-3"><strong>Firma:</strong></div>
                        <div class="col-sm-8"><?php echo do_shortcode('[Writevalues cookievalue="rechnung_firma"]')?></div>
                    </div>
                    <div class="row">
                        <div class="col-sm-3"><strong>Straße:</strong></div>
                        <div class="col-sm-8"><?php echo do_shortcode('[Writevalues cookievalue="rechnung_str"]')?></div>
                    </div>
                    <div class="row">
                        <div class="col-sm-3"><strong>PLZ/Ort:</strong></div>
                        <div class="col-sm-8"><?php echo do_shortcode('[Writevalues cookievalue="rechnung_plz"]')?> <?php echo do_shortcode('[Writevalues cookievalue="rechnung_ort"]')?></div>
                    </div>
                </fieldset>
                <?php } ?>
            </fieldset>

            <fieldset class="well">
                <legend>
                    Teilnehmer
                    <a href="/teilnehmer" class="btn btn-default btn-xs pull-right">Ändern</a>
                </legend>
                <?php
                $cookievalues = $_COOKIE["cf7msm_posted_data"];
              //  print_r (stripslashes($cookievalues));
                ?>
                <fieldset class="well well-inner">
                    <legend>Teilnehmer 1</legend>
                    <div class="row">
                        <div class="col-sm-3"><strong>Vor- und Nachname:</strong></div>
                        <div class="col-sm-8"><?php echo do_shortcode('[Writevalues cookievalue="teilnehmer_anrede"]')?> <?php echo do_shortcode('[Writevalues cookievalue="teilnehmer_vorname"]')?> <?php echo do_shortcode('[Writevalues cookievalue="teilnehmer_nachname"]')?></div>
                    </div>
                    <div class="row">
                        <div class="col-sm-3"><strong>E-Mail Adresse:</strong></div>
                        <div class="col-sm-8"><?php echo do_shortcode('[Writevalues cookievalue="teilnehmer_email"]')?></div>
                    </div>
                    <?php if ($tnHandy != 0) { ?>
                    <div class="row">
                        <div class="col-sm-3"><strong>Handy-Nr.:</strong></div>
                        <div class="col-sm-8"><?php echo do_shortcode('[Writevalues cookievalue="Handy_Nr"]')?></div>
                    </div>
                    <?php } ?>
                </fieldset>

                <?php if (!empty($tn2)) { ?>
                <fieldset class="well well-inner">
                    <legend>Teilnehmer 2</legend>
                    <div class="row">
                        <div class="col-sm-3"><strong>Vor- und Nachname:</strong></div>
                        <div class="col-sm-8"><?php echo do_shortcode('[Writevalues cookievalue="tn2_anrede"]')?> <?php echo do_shortcode('[Writevalues cookievalue="tn2_vorname"]')?> <?php echo do_shortcode('[Writevalues cookievalue="tn2_nachname"]')?></div>
                    </div>
                    <div class="row">
                        <div class="col-sm-3"><strong>E-Mail Adresse:</strong></div>
                        <div class="col-sm-8"><?php echo do_shortcode('[Writevalues cookievalue="tn2_email"]')?></div>
                    </div>
                    <?php if ($tn2Handy != 0) { ?>
                    <div class="row">
                        <div class="col-sm-3"><strong>Handy-Nr.:</strong></div>
                        <div class="col-sm-8"><?php echo do_shortcode('[Writevalues cookievalue="tn2_Handy_Nr"]')?></div>
                    </div>
                    <?php } ?>
                </fieldset>
                <?php } ?>

                <?php if (!empty($tn3)) { ?>
                <fieldset class="well well-inner">
                    <legend>Teilnehmer 3</legend>
                    <div class="row">
                        <div class="col-sm-3"><strong>Vor- und Nachname:</strong></div>
                        <div class="col-sm-8"><?php echo do_shortcode('[Writevalues cookievalue="tn3_anrede"]')?> <?php echo do_shortcode('[Writevalues cookievalue="tn3_vorname"]')?> <?php echo do_shortcode('[Writevalues cookievalue="tn3_nachname"]')?></div>
                    </div>
                    <div class="row">
                        <div class="col-sm-3"><strong>E-Mail Adresse:</strong></div>
                        <div class="col-sm-8"><?php echo do_shortcode('[Writevalues cookievalue="tn3_email"]')?></div>
                    </div>
                    <?php if ($tn3Handy != 0) { ?>
                    <div class="row">
                        <div class="col-sm-3"><strong>Handy-Nr.:</strong></div>
                        <div class="col-sm-8"><?php echo do_shortcode('[Writevalues cookievalue="tn3_Handy_Nr"]')?></div>
                    </div>
                    <?php } ?>
                </fieldset>
                <?php } ?>
            </fieldset>

            <fieldset class="well">
                <legend>
                    Zertifizierung
                    <a href="/serviceleistungen" class="btn btn-default btn-xs pull-right">Ändern</a>
                </legend>
                <?php if (!empty($zertifizierung)) { ?>
                <p>GFU-Zertifizierung zusätzlich gebucht</p>

                <p>
                Prüfungsgebühr: <strong><span class="zpreis"></span>,00 EUR</strong> pro Person zzgl. MwSt.<br>
                Die Prüfungsgebühr ist im Seminarpreis nicht enthalten.
                </p>
                <?php } else { ?>
                    <p><i class="fa fa-times text-danger" aria-hidden="true"></i> Keine Zertifizierung gebucht</p>
                <?php } ?>
            </fieldset>

            <?php if ($parkplatz != 0 OR $shuttle != 0 OR $hotelreservierung != 0) { ?>
            <fieldset class="well">
                <legend>
                    Kostenlose Serviceleistungen
                    <a href="/serviceleistungen" class="btn btn-default btn-xs pull-right">Ändern</a>
                </legend>

                <?php if ($parkplatz != 0) { ?>
                    <i class="fa fa-check ok-green" aria-hidden="true"></i>
                <?php } else { ?>
                    <i class="fa fa-times text-danger" aria-hidden="true"></i>
                <?php } ?>
                Parkplatzreservierung
                <br>
                <?php if ($shuttle != 0) { ?>
                    <i class="fa fa-check ok-green" aria-hidden="true"></i>
                <?php } else { ?>
                    <i class="fa fa-times text-danger" aria-hidden="true"></i>
                <?php } ?>
                Shuttle-Service
                <?php if ($hotelreservierung == 0) { ?>
                    <br>
                    <i class="fa fa-times text-danger" aria-hidden="true"></i>
                    Hotelreservierung
                <?php } ?>

                <?php if ($hotelreservierung != 0) { ?>
                <br><br>
                <fieldset class="well well-inner">
                    <legend>Hotelreservierung</legend>

                    <strong>Anreise:</strong> <?php echo do_shortcode('[Writevalues cookievalue="anreise"]') ?>&nbsp;&nbsp;
                    <strong>Abreise:</strong> <?php echo do_shortcode('[Writevalues cookievalue="abreise"]') ?>

                    <?php if ($hotel == 'maritim') { ?>
                        <a href="https://www.gfu.net/gfuhotel.html">
                            <h4>MARITIM Hotel Köln</h4>
                        </a>
                        <p>
                            Gehobene Business Class<br>
                            Am Rheinufer, nur wenige Schritte von der historischen Altstadt und dem Kölner Dom entfernt, liegt das Maritim Hotel, das sich auch durch seine beeindruckende Architektur gekonnt in Szene setzt. Mehrere elegante Restaurants und Bistros, darunter das Dachgartenrestaurant "Bellevue", das Bistro "La Galerie", das Restaurant "Rôtisserie", die Bier- und Weinstube "Kölsche Stuff", das Café "Heumarkt" sowie die Piano-Bar mit abendlichem Live-Programm sorgen fürs leibliche Wohl der Gäste.
                        </p>
                    <?php } else if ($hotel == 'motelone') { ?>
                        <a href="https://www.gfu.net/gfuhotel.html">
                            <h4>Motel One am Waidmarkt</h4>
                        </a>
                        <p>
                            Economy Class<br>
                            Freuen Sie sich bei Ihrem Aufenthalt in diesem Motel One in Köln auf modern eingerichtete Zimmer, freundliches Personal und absolut zentrale Lage. Sie erreichen in kürzester Zeit die beliebten Einkaufsmeilen Hohe Straße oder Schildergasse. Auch die Altstadt und der Rhein sind sehr nahe.
                        </p>
                    <?php } else if ($hotel == 'allegro') { ?>
                        <a href="https://www.gfu.net/gfuhotel.html">
                            <h4>Hotel Allegro</h4>
                        </a>
                        <p>
                            Economy Class<br>
                            Das 3 Sterne Stadthotel liegt sehr zentral am Rhein, unmittelbar neben dem Maritim-Hotel. Zur Innenstadt sind es wenige Gehminuten, die Altstadt erreichen Sie über die Rheinuferpromenade.
                        </p>
                    <?php } else if ($hotel == 'cityclass') { ?>
                        <a href="https://www.gfu.net/gfuhotel.html">
                            <h4>CityClass Hotel: Capirce am Dom</h4>
                        </a>
                        <p>
                            Economy Class<br>
                            Das 3-Sterne-Superior-Hotel liegt im Herzen der Altstadt - in wenigen Schritten sind Sie am Dom, am Rhein und in der Fußgängerzone. Der kleine aber feine Wellnessbereich steht Ihnen kostenlos zur Verfügung. WLAN steht für Sie kostenlos in den Zimmern zur Verfügung.
                        </p>
                    <?php } else if ($hotel == 'mauritius') { ?>
                        <a href="https://www.gfu.net/gfuhotel.html">
                            <h4>Mauritius Komfort Hotel</h4>
                        </a>
                        <p>
                            Economy Class<br>
                            Das Mauritius Komfort Hotel liegt im Herzen von Köln, in direkter Nähe von Heumarkt und der Altstadt, Kölner Dom, Deutzer Brücke, Köln Messe, sowie den Fußgängerzonen Hohe Strasse und Schildergasse.
                        </p>
                    <?php } ?>
                </fieldset>
                <?php } ?>
            </fieldset>
            <?php } ?>

            <fieldset class="well">
                <legend>
                    Kommentar
                    <a href="/serviceleistungen" class="btn btn-default btn-xs pull-right">Ändern</a>
                </legend>
                <?php if (!empty($kommentar)) { ?>
                    <?php echo $kommentar ?>
                <?php } else { ?>
                    <i class="fa fa-times text-danger" aria-hidden="true"></i>
                    Kein Kommentar hinzugefügt
                <?php } ?>
            </fieldset>
            <a href="/serviceleistungen" class="btn btn-default pull-left">Zurück</a>
            <a href="/wp-content/plugins/gfu-form-receiver-sender/finalizer.php" class="btn btn-lg btn-cta pull-right">Jetzt buchen</a>
    </div>
    </div>
    <div class="tab-pane" id="vormerken">
        <?php include 'form-vormerken.php'; ?>
    </div>

</div>

<?php
get_footer();
?>