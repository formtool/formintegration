<form name="fRes" action="" method="POST" class="form-horizontal form_reservierung">
        <div class="well">
        <div class="form-group">
            <label class="control-label col-sm-3">Firmenname: <span class="required">*</span></label>
            <div class="col-sm-9 container_form-control">
            <input type="text" name="firma" class="form-control">
            </div>
        </div>
        
        <div class="form-group">
            <label class="control-label col-sm-3">Ansprechpartner: <span class="required">*</span></label>
            <div class="col-sm-9">
            <div class="row gutter-sm">
                <div class="col-sm-2 container_form-control">
                <select name="ap_anrede" id="salutation" class="form-control">
                    <option value="Frau">Frau</option>
                    <option value="Herr">Herr</option>
                </select>
                </div>
                <div class="col-xs-6 col-sm-5 container_form-control">
                <input type="text" name="ap_vorname" class="form-control">
                </div>
                <div class="col-xs-6 col-sm-5 container_form-control">
                <input type="text" name="ap_nachname" class="form-control">
                </div>
            </div>
            </div>
        </div>
        
        <div class="form-group">
            <label class="control-label col-sm-3">Telefon:</label>
            <div class="col-sm-9 container_form-control">
            <input type="tel" name="telefon" class="form-control">
            </div>
        </div>
        
        <div class="form-group">
            <label class="control-label col-sm-3">E-Mail Adresse: <span class="required">*</span></label>
            <div class="col-sm-9 container_form-control">
            <input type="text" name="ap_email" class="form-control">
            </div>
        </div>
        
        <div class="form-group">
            <label class="control-label col-sm-3">Teilnehmer: <span class="required">*</span></label>
            <div class="col-sm-9">
            <div class="row gutter-sm">
                <div class="col-sm-2 container_form-control">
                <select name="teilnehmer_anrede" id="salutation" class="form-control">
                    <option value="Frau">Frau</option>
                    <option value="Herr">Herr</option>
                </select>
                </div>
                <div class="col-xs-6 col-sm-5 container_form-control">
                <input type="text" name="teilnehmer_vorname" class="form-control">
                </div>
                <div class="col-xs-6 col-sm-5 container_form-control">
                <input type="text" name="teilnehmer_nachname" class="form-control">
                </div>
            </div>
            </div>
        </div>
        
        <ol class="participants" id="tnContainerReserve"></ol>

        <script type="text/x-tmpl" id="participant">
            <li class="participants__item collapse">
                <fieldset class="well well-inner">
                <legend>
                    Teilnehmer <span class="participants__count">{%=o.count%}</span>
                    {% if (o.kDrei == 0) { %}
                    <span class="participants__kdrei">(kostenlose Teilnahme)</span>
                    {% } %}
                    <button type="button" class="btn btn-sm btn-danger removeParticipant" style="padding: 2px 6px;float: right;margin:-2px -2px 0 0;">
                    <i class="fa fa-times" aria-hidden="true"></i>
                    </button>
                </legend>
                <div class="clearfix"></div>
                <div class="form-group">
                    <label class="control-label col-sm-3">
                    Vor- und Nachname:
                    </label>
                    <div class="col-sm-9">
                    <div class="row gutter-sm">
                        <div class="col-sm-2 container_form-control">
                        <select name="{%=o.prefix%}_anrede" class="form-control">
                            <option value="Frau">Frau</option>
                            <option value="Herr">Herr</option>
                        </select>
                        </div>
                        <div class="col-xs-6 col-sm-5 container_form-control">
                        <input type="text" name="{%=o.prefix%}_vorname" size="40" class="form-control">
                        </div>
                        <div class="col-xs-6 col-sm-5 container_form-control">
                        <input type="text" name="{%=o.prefix%}_nachname" size="40" class="form-control">
                        </div>
                    </div>
                    </div>
                </div>
                </fieldset>
            </li>
        </script>

        <div class="row b-row">
            <div class="col-sm-12">
                <button type="button" class="btn btn-success" id="addParticipantReserve" onclick="return!1">
                    <i class="fa fa-plus" aria-hidden="true"></i> Teilnehmer hinzufügen 
                </button>
                <div class="free-hint free-hint-bubble">
                    <span class="tft-icon"></span> Der dritte Mitarbeiter nimmt kostenlos teil
                </div>
            </div>
        </div>
        </div>
        
        <a href="/vormerken-ubersicht" class="btn btn-cta pull-right" id="vormerkenBtn">Weiter</a>
    </form>