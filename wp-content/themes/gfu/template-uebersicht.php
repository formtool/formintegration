<?php
/**
 * Template Name: Template Vormerken (Zusammenfassung)
 */
get_header();
?>
<div class="sem-overview">
    <div class="sem-overview__title">
        <span class="sem-overview__label">Seminar</span>
        <span class="semtitel"></span><?php echo do_shortcode('[Param params="S"]'); ?> (<span
            class="semid"><?php echo do_shortcode('[Param params="I"]'); ?></span>)
    </div>

    <div class="sem-overview__row">
        <div class="sem-overview__cell">
            <span class="sem-overview__label">Termin</span>
            <span class="semtermin"><?php echo do_shortcode('[Param params="T"]'); ?></span>
        </div>

        <div class="sem-overview__cell">
            <span class="sem-overview__label">Ort</span>
            <span class="semort"><?php echo do_shortcode('[Param params="ort"]'); ?></span>
        </div>

        <div class="sem-overview__cell">
            <span class="sem-overview__label pLabel">Preis</span>
            <span class="price">
		<span class="sempreis p"><?php echo do_shortcode('[Param params="G"]'); ?></span> &euro;
                <span class="asterisk"></span>
	</span>
            <span class="sem-overview__mwst">(zzgl. Mwst.)</span>
        </div>
    </div>
</div>
<div class="price-included">
    <span class="asterisk"></span> <strong>Im Preis enthalten:</strong>
    <div class="row">
        <div class="col-sm-6">
            <ul class="list">
                <li>Voll ausgestatteter Arbeitsplatz pro Teilnehmer</li>
                <li>Fachbuch zum Seminar</li>
                <li>Teilnahmezertifikat</li>
                <li>Kostenloser persönlicher Parkplatz</li>
            </ul>
        </div>
        <div class="col-sm-6">
            <ul class="list">
                <li>Kostenloser Shuttle-Service</li>
                <li>Frühstück, Snacks und Getränke ganztägig</li>
                <li>Mittagessen im eigenen Restaurant, täglich 6 Menüs, auch vegetarisch</li>
            </ul>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-sm-6">
        <h4>Buchen</h4>
        <p>Buchen Sie jetzt! Das Seminar findet garantiert statt. Die Buchung kann bis zum letzten Arbeitstag vor der
            Schulung kostenlos storniert werden.<br>
            Sie können auch per <a href="#" id="get-print">Faxformular</a> oder Telefon buchen.
        </p>
    </div>
    <div class="col-sm-6">
        <h4>Unverbindlich vormerken</h4>
        <p>Sichern Sie sich Ihren Seminarplatz schon vor der Buchung! So können Sie in Ruhe alle Modalitäten regeln. Das
            Vormerken des Kurses ist für Sie nicht verbindlich. Die Buchung sollte aber innerhalb von 10 Tagen erfolgen,
            da wir Ihren Platz danach wieder freigeben müssen.</p>
    </div>
</div>

<ul class="nav nav-tabs nav-anmeldung">
    <li id="trigger-buchen">
        <a href="#buchen" aria-expanded="true" data-toggle="tab">Buchen</a>
    </li>
    <li id="trigger-vormerken" class="active">
        <a href="#vormerken" aria-expanded="false" data-toggle="tab">Unverbindlich vormerken</a>
    </li>
</ul>
<br>
<div class="tab-content form_anmeldung anmeldung-offen form-horizontal">
    <div class="tab-pane" id="buchen">
    <?php echo do_shortcode('[formSteps step="1"]'); ?>
    <?php echo do_shortcode('[contact-form-7 id="4" title="Contact form 1"]'); ?>
    </div>
    <div class="tab-pane active" id="vormerken">
        <div class="form form--multistep">
            <fieldset class="well">
                <legend>Firma</legend>
                <div class="row">
                    <div class="col-sm-3"><strong>Firma:</strong></div>
                    <div class="col-sm-8"><?php echo do_shortcode('[Writevalues cookievalue="firm"]')?></div>
                </div>
                <div class="row">
                    <div class="col-sm-3"><strong>Ansprechpartner:</strong></div>
                    <div class="col-sm-8"><?php echo do_shortcode('[Writevalues cookievalue="ap_anrede"]')?> <?php echo do_shortcode('[Writevalues cookievalue="ap_vorname"]')?> <?php echo do_shortcode('[Writevalues cookievalue="ap_nachname"]')?></div>
                </div>
                <div class="row">
                    <div class="col-sm-3"><strong>Telefon:</strong></div>
                    <div class="col-sm-8"><?php echo do_shortcode('[Writevalues cookievalue="telefon"]')?></div>
                </div>
                <div class="row">
                    <div class="col-sm-3"><strong>E-Mail Adresse:</strong></div>
                    <div class="col-sm-8"><?php echo do_shortcode('[Writevalues cookievalue="ap_email"]')?></div>
                </div>
            </fieldset>

            <fieldset class="well">
                <legend>Teilnehmer</legend>
                <?php
                $cookievalues = $_COOKIE["cf7msm_posted_data"];
                $tn3 = do_shortcode('[Writevalues cookievalue="tn3_vorname"]');
                $tn2 = do_shortcode('[Writevalues cookievalue="tn2_vorname"]');
                ?>
                <fieldset class="well well-inner">
                    <legend>Teilnehmer 1</legend>
                    <div class="row">
                        <div class="col-sm-3"><strong>Vor- und Nachname:</strong></div>
                        <div class="col-sm-8"><?php echo do_shortcode('[Writevalues cookievalue="teilnehmer_anrede"]')?> <?php echo do_shortcode('[Writevalues cookievalue="teilnehmer_vorname"]')?> <?php echo do_shortcode('[Writevalues cookievalue="teilnehmer_nachname"]')?></div>
                    </div>
                </fieldset>

                <?php if (!empty($tn2)) { ?>
                <fieldset class="well well-inner">
                    <legend>Teilnehmer 2</legend>
                    <div class="row">
                        <div class="col-sm-3"><strong>Vor- und Nachname:</strong></div>
                        <div class="col-sm-8"><?php echo do_shortcode('[Writevalues cookievalue="tn2_anrede"]')?> <?php echo do_shortcode('[Writevalues cookievalue="tn2_vorname"]')?> <?php echo do_shortcode('[Writevalues cookievalue="tn2_nachname"]')?></div>
                    </div>
                </fieldset>
                <?php } ?>

                <?php if (!empty($tn3)) { ?>
                <fieldset class="well well-inner">
                    <legend>Teilnehmer 3</legend>
                    <div class="row">
                        <div class="col-sm-3"><strong>Vor- und Nachname:</strong></div>
                        <div class="col-sm-8"><?php echo do_shortcode('[Writevalues cookievalue="tn3_anrede"]')?> <?php echo do_shortcode('[Writevalues cookievalue="tn3_vorname"]')?> <?php echo do_shortcode('[Writevalues cookievalue="tn3_nachname"]')?></div>
                    </div>
                </fieldset>
                <?php } ?>
            </fieldset>

            <a href="/#vormerken" class="btn btn-default pull-left">Zurück</a>
            <a href="/wp-content/plugins/gfu-form-receiver-sender/finalizer.php" class="btn btn-lg btn-cta pull-right">Jetzt vormerken</a>
    </div>
    </div>

</div>

<?php
get_footer();
?>