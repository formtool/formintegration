<?php
/**
 * Template Name: Anmeldung Offen
 */
if (isset($_COOKIE['booking-finalized'])) {
header("location: https://www.gfu.net/bereitsgebucht.html");
}
get_header();
?>

<div class="sem-overview">
    <div class="sem-overview__title">
        <span class="sem-overview__label">Seminar</span>
        <span class="semtitel"></span><?php echo do_shortcode('[Param params="S"]'); ?> (<span
            class="semid"><?php echo do_shortcode('[Param params="I"]'); ?></span>)
    </div>

    <div class="sem-overview__row">
        <div class="sem-overview__cell">
            <span class="sem-overview__label">Termin</span>
            <span class="semtermin"><?php echo do_shortcode('[Param params="T"]'); ?></span>
        </div>

        <div class="sem-overview__cell">
            <span class="sem-overview__label">Ort</span>
            <span class="semort"><?php echo do_shortcode('[Param params="ort"]'); ?></span>
        </div>

        <div class="sem-overview__cell">
            <span class="sem-overview__label pLabel">Preis</span>
            <span class="price">
		<span class="sempreis p"><?php echo do_shortcode('[Param params="G"]'); ?></span> &euro;
                <span class="asterisk"></span>
	</span>
            <span class="sem-overview__mwst">(zzgl. Mwst.)</span>
        </div>
    </div>
</div>
<div class="price-included">
    <span class="asterisk"></span> <strong>Im Preis enthalten:</strong>
    <div class="row">
        <div class="col-sm-6">
            <ul class="list">
                <li>Voll ausgestatteter Arbeitsplatz pro Teilnehmer</li>
                <li>Fachbuch zum Seminar</li>
                <li>Teilnahmezertifikat</li>
                <li>Kostenloser persönlicher Parkplatz</li>
            </ul>
        </div>
        <div class="col-sm-6">
            <ul class="list">
                <li>Kostenloser Shuttle-Service</li>
                <li>Frühstück, Snacks und Getränke ganztägig</li>
                <li>Mittagessen im eigenen Restaurant, täglich 6 Menüs, auch vegetarisch</li>
            </ul>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-sm-6">
        <h4>Buchen</h4>
        <p>Buchen Sie jetzt! Das Seminar findet garantiert statt. Die Buchung kann bis zum letzten Arbeitstag vor der
            Schulung kostenlos storniert werden.<br>
            Sie können auch per <a href="#" id="get-print">Faxformular</a> oder Telefon buchen.
        </p>
    </div>
    <div class="col-sm-6">
        <h4>Unverbindlich vormerken</h4>
        <p>Sichern Sie sich Ihren Seminarplatz schon vor der Buchung! So können Sie in Ruhe alle Modalitäten regeln. Das
            Vormerken des Kurses ist für Sie nicht verbindlich. Die Buchung sollte aber innerhalb von 10 Tagen erfolgen,
            da wir Ihren Platz danach wieder freigeben müssen.</p>
    </div>
</div>

<ul class="nav nav-tabs nav-anmeldung">
    <li id="trigger-buchen">
        <a href="#buchen" aria-expanded="true" data-toggle="tab">Buchen</a>
    </li>
    <li id="trigger-vormerken" class="active">
        <a href="#vormerken" aria-expanded="false" data-toggle="tab">Unverbindlich vormerken</a>
    </li>
</ul>
<br>
<div class="tab-content form_anmeldung anmeldung-offen form-horizontal">
    <div class="tab-pane" id="buchen">
    <ul class="nav nav-tabs nav-anmeldung">
        <li id="trigger-buchen">
            <a href="#buchen" aria-expanded="true" data-toggle="tab">Buchen</a>
        </li>
        <li id="trigger-vormerken" class="active">
            <a href="#vormerken" aria-expanded="false" data-toggle="tab">Unverbindlich vormerken</a>
        </li>
    </ul>
    </div>
    <div class="tab-pane" id="buchen">
        <?php echo do_shortcode('[formSteps step="1"]'); ?>
        <?php echo do_shortcode('[contact-form-7 id="4" title="Contact form 1"]'); ?>
    </div>
    <div class="tab-pane active" id="vormerken">
    <form name="fRes" action="" method="POST" class="form-horizontal form_reservierung">
        <div class="well">
        <div class="form-group">
            <label class="control-label col-sm-3">Firmenname: <span class="required">*</span></label>
            <div class="col-sm-9 container_form-control">
            <input type="text" name="firma" class="form-control">
            </div>
        </div>
        
        <div class="form-group">
            <label class="control-label col-sm-3">Ansprechpartner: <span class="required">*</span></label>
            <div class="col-sm-9">
            <div class="row gutter-sm">
                <div class="col-sm-2 container_form-control">
                <select name="ap_anrede" id="salutation" class="form-control">
                    <option value="Frau">Frau</option>
                    <option value="Herr">Herr</option>
                </select>
                </div>
                <div class="col-xs-6 col-sm-5 container_form-control">
                <input type="text" name="ap_vorname" class="form-control">
                </div>
                <div class="col-xs-6 col-sm-5 container_form-control">
                <input type="text" name="ap_nachname" class="form-control">
                </div>
            </div>
            </div>
        </div>
        
        <div class="form-group">
            <label class="control-label col-sm-3">Telefon:</label>
            <div class="col-sm-9 container_form-control">
            <input type="tel" name="telefon" class="form-control">
            </div>
        </div>
        
        <div class="form-group">
            <label class="control-label col-sm-3">E-Mail Adresse: <span class="required">*</span></label>
            <div class="col-sm-9 container_form-control">
            <input type="text" name="ap_email" class="form-control">
            </div>
        </div>
        
        <div class="form-group">
            <label class="control-label col-sm-3">Teilnehmer: <span class="required">*</span></label>
            <div class="col-sm-9">
            <div class="row gutter-sm">
                <div class="col-sm-2 container_form-control">
                <select name="teilnehmer_anrede" id="salutation" class="form-control">
                    <option value="Frau">Frau</option>
                    <option value="Herr">Herr</option>
                </select>
                </div>
                <div class="col-xs-6 col-sm-5 container_form-control">
                <input type="text" name="teilnehmer_vorname" class="form-control">
                </div>
                <div class="col-xs-6 col-sm-5 container_form-control">
                <input type="text" name="teilnehmer_nachname" class="form-control">
                </div>
            </div>
            </div>
        </div>
        
        <ol class="participants" id="tnContainerReserve"></ol>

        <script type="text/x-tmpl" id="participant">
            <li class="participants__item collapse">
                <fieldset class="well well-inner">
                <legend>
                    Teilnehmer <span class="participants__count">{%=o.count%}</span>
                    {% if (o.kDrei == 0) { %}
                    <span class="participants__kdrei">(kostenlose Teilnahme)</span>
                    {% } %}
                    <button type="button" class="btn btn-sm btn-danger removeParticipant" style="padding: 2px 6px;float: right;margin:-2px -2px 0 0;">
                    <i class="fa fa-times" aria-hidden="true"></i>
                    </button>
                </legend>
                <div class="clearfix"></div>
                <div class="form-group">
                    <label class="control-label col-sm-3">
                    Vor- und Nachname:
                    </label>
                    <div class="col-sm-9">
                    <div class="row gutter-sm">
                        <div class="col-sm-2 container_form-control">
                        <select name="{%=o.prefix%}_anrede" class="form-control">
                            <option value="Frau">Frau</option>
                            <option value="Herr">Herr</option>
                        </select>
                        </div>
                        <div class="col-xs-6 col-sm-5 container_form-control">
                        <input type="text" name="{%=o.prefix%}_vorname" size="40" class="form-control">
                        </div>
                        <div class="col-xs-6 col-sm-5 container_form-control">
                        <input type="text" name="{%=o.prefix%}_nachname" size="40" class="form-control">
                        </div>
                    </div>
                    </div>
                </div>
                </fieldset>
            </li>
        </script>

        <div class="row b-row">
            <div class="col-sm-12">
                <button type="button" class="btn btn-success" id="addParticipantReserve" onclick="return!1">
                    <i class="fa fa-plus" aria-hidden="true"></i> Teilnehmer hinzufügen 
                </button>
                <div class="free-hint free-hint-bubble">
                    <span class="tft-icon"></span> Der dritte Mitarbeiter nimmt kostenlos teil
                </div>
            </div>
        </div>
        </div>
        
        <a href="/vormerken-ubersicht" class="btn btn-cta pull-right" id="vormerkenBtn">Weiter</a>
    </form>
    </div>
</div>

<?php
get_footer();

?>
