<?php

require_once('../../../wp-load.php');
function checkCookie()
{
    $urlraw = $_SERVER['REQUEST_URI'];
    $url = strstr($urlraw, '?');
    if (isset($_COOKIE['booking-test'])) {
        getFromParamToDb();
        getFromDbToCookie();
    } else {
        noCookieActions();
        header("Location:https://www.gfu.net/anmeldung.html$url");
    }
}

function noCookieActions()
{
    if (session_id() == '' || !isset($_SESSION)) {
        session_start();
    };
    global $wpdb;
    $identifier = session_id();

    if (!empty($_SERVER['HTTP_CLIENT_IP']))   //check ip from share internet
    {
        $ip = $_SERVER['HTTP_CLIENT_IP'];
    } elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR']))   //to check ip is pass from proxy
    {
        $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
    } else {
        $ip = $_SERVER['REMOTE_ADDR'];
    }

    $browserinfo = $_SERVER['HTTP_USER_AGENT'];

    if (isset ($_SERVER['HTTP_REFERER'])) {
        $isreferrer = $_SERVER['HTTP_REFERER'];
    } else {
        $isreferrer = 'no-referrer';
    }
    $urlraw = $_SERVER['REQUEST_URI'];
    $url = strstr($urlraw, '?');
    $iscookie = '0';
    $submission = session_id();

    // This is nececcary because of windows encoding on gfu.net site
    $decodeort = mb_convert_encoding($_GET['ort'], "UTF-8", "Windows-1252");
    $decodeS = mb_convert_encoding($_GET['S'], "UTF-8", "Windows-1252");
    $decodeT = mb_convert_encoding($_GET['T'], "UTF-8", "Windows-1252");
    $decodeG = mb_convert_encoding($_GET['G'], "UTF-8", "Windows-1252");
    $decodeI = mb_convert_encoding($_GET['I'], "UTF-8", "Windows-1252");
    $decodeD = mb_convert_encoding($_GET['D'], "UTF-8", "Windows-1252");
    $decodeA = mb_convert_encoding($_GET['A'], "UTF-8", "Windows-1252");
    $decodeFBP = mb_convert_encoding($_GET['FBP'], "UTF-8", "Windows-1252");
    $decodefolgeseminar = mb_convert_encoding($_GET['folgeseminar'], "UTF-8", "Windows-1252");
    $decodecat = mb_convert_encoding($_GET['cat'], "UTF-8", "Windows-1252");
    $decodekDrei = mb_convert_encoding($_GET['kDrei'], "UTF-8", "Windows-1252");
    $decodehKnapp = mb_convert_encoding($_GET['hKnapp'], "UTF-8", "Windows-1252");
    $optimice1 = mb_convert_encoding($_GET['utm_expid'], "UTF-8", "Windows-1252");
    $optimice2 = mb_convert_encoding($_GET['utm_referrer'], "UTF-8", "Windows-1252");
    $zerti = mb_convert_encoding($_GET['zpreis'], "UTF-8", "Windows-1252");
    //end conversion

    global $wpdb;
                $wpdb->insert('forms_sender_submitted',
                    array(
                        'idsubmitted' => '',
                        'session' => $submission,
                        'ipaddress' => $ip,
                        'browserinfo' => $browserinfo,
                        'referrer' => $isreferrer,
                        'cookie' => $iscookie,
                        'urlraw' => $url,
                        'S' => $decodeS,
                        'T' => $decodeT,
                        'G' => $decodeG,
                        'I' => $decodeI,
                        'D' => $decodeD,
                        'A' => $decodeA,
                        'FBP' => $decodeFBP,
                        'folgeseminar' => $decodefolgeseminar,
                        'cat' => $decodecat,
                        'kDrei' => $decodekDrei,
                        'hKnapp' => $decodehKnapp,
                        "ort" => $decodeort,
                        "zerti" => $zerti,
                        'firm' => '',
                        'str' => '',
                        'plz' => '',
                        'firmort' => '',
                        'ap_anrede' => '',
                        'ap_vorname' => '',
                        'ap_nachname' => '',
                        'telefon' => '',
                        'ap_email' => '',
                        'altbill' => '',
                        'rechnung_firma' => '',
                        'rechnung_str' => '',
                        'rechnung_plz' => '',
                        'rechnung_ort' => '',
                        'teilnehmer_anrede' => '',
                        'teilnehmer_vorname' => '',
                        'teilnehmer_nachname' => '',
                        'teilnehmer_email' => '',
                        'teilnehmer_mobile' => '',
                        'tn2_anrede' => '',
                        'tn2_vorname' => '',
                        'tn2_nachname' => '',
                        'tn2_email' => '',
                        'tn2_mobile' => '',
                        'tn3_anrede' => '',
                        'tn3_vorname' => '',
                        'tn3_nachname' => '',
                        'tn3_email' => '',
                        'tn3_mobile' => '',
                        'parkplatz' => '',
                        'shuttle' => '',
                        'hotelreservierung' => '',
                        'hotel' => '',
                        'anreise' => '',
                        'abreise' => '',
                        'sonstiges1' => ''
                    ));
                $wpdb->query("DELETE FROM forms_sender_receiver WHERE submission = '$identifier'");
                // header("Location:https://buchungen.gfu.net/leider-keine-cookies/$url");

}

function getFromParamToDb()
{
    if (session_id() == '' || !isset($_SESSION)) {
        session_start();
    };

    if (!empty($_SERVER['HTTP_CLIENT_IP']))   //check ip from share internet
    {
        $ip = $_SERVER['HTTP_CLIENT_IP'];
    } elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR']))   //to check ip is pass from proxy
    {
        $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
    } else {
        $ip = $_SERVER['REMOTE_ADDR'];
    }

    $browserinfo = $_SERVER['HTTP_USER_AGENT'];

    if (isset ($_SERVER['HTTP_REFERER'])) {
        $isreferrer = $_SERVER['HTTP_REFERER'];
    } else {
        $isreferrer = 'no-referrer';
    }
    $urlraw = $_SERVER['REQUEST_URI'];
    $url = strstr($urlraw, '?');
    $submission = session_id();
    $iscookie = '1';

    // This is nececcary because of windows encoding on gfu.net site
    $decodeort = mb_convert_encoding($_GET['ort'], "UTF-8", "Windows-1252");
    $decodeS = mb_convert_encoding($_GET['S'], "UTF-8", "Windows-1252");
    $decodeT = mb_convert_encoding($_GET['T'], "UTF-8", "Windows-1252");
    $decodeG = mb_convert_encoding($_GET['G'], "UTF-8", "Windows-1252");
    $decodeI = mb_convert_encoding($_GET['I'], "UTF-8", "Windows-1252");
    $decodeD = mb_convert_encoding($_GET['D'], "UTF-8", "Windows-1252");
    $decodeA = mb_convert_encoding($_GET['A'], "UTF-8", "Windows-1252");
    $decodeFBP = mb_convert_encoding($_GET['FBP'], "UTF-8", "Windows-1252");
    $decodefolgeseminar = mb_convert_encoding($_GET['folgeseminar'], "UTF-8", "Windows-1252");
    $decodecat = mb_convert_encoding($_GET['cat'], "UTF-8", "Windows-1252");
    $decodekDrei = mb_convert_encoding($_GET['kDrei'], "UTF-8", "Windows-1252");
    $decodehKnapp = mb_convert_encoding($_GET['hKnapp'], "UTF-8", "Windows-1252");
    $optimice1 = mb_convert_encoding($_GET['utm_expid'], "UTF-8", "Windows-1252");
    $optimice2 = mb_convert_encoding($_GET['utm_referrer'], "UTF-8", "Windows-1252");
    $zerti = mb_convert_encoding($_GET['zpreis'], "UTF-8", "Windows-1252");
    $iszert = mb_convert_encoding($_GET['z'], "UTF-8", "Windows-1252");
    //end conversion

    global $wpdb;
    $identifier = session_id();
    $checkresults = $wpdb->get_results("SELECT submission FROM forms_sender_receiver WHERE submission = '$identifier'");

    if (!empty ($checkresults)) {
        $wpdb->update('forms_sender_receiver',
            array(
                'ipaddress' => $ip,
                'browserinfo' => $browserinfo,
                'cookie' => $iscookie,
                'referrer' => $isreferrer,
                'urlraw' => $url,
                'S' => $decodeS,
                'T' => $decodeT,
                'G' => $decodeG,
                'I' => $decodeI,
                'D' => $decodeD,
                'A' => $decodeA,
                'FBP' => $decodeFBP,
                'folgeseminar' => $decodefolgeseminar,
                'cat' => $decodecat,
                'kDrei' => $decodekDrei,
                'hKnapp' => $decodehKnapp,
                "ort" => $decodeort,
                "optimice1" => $optimice1,
                "optimice2" => $optimice2,
                "zerti" => $zerti,
                "iszert"  => $iszert

            ),
            array(
                'submission' => $submission,
            )
        );
    } else {
        $wpdb->insert('forms_sender_receiver',
            array(
                'id' => '',
                'submission' => $submission,
                'ipaddress' => $ip,
                'browserinfo' => $browserinfo,
                'cookie' => $iscookie,
                'referrer' => $isreferrer,
                'urlraw' => $url,
                'S' => $decodeS,
                'T' => $decodeT,
                'G' => $decodeG,
                'I' => $decodeI,
                'D' => $decodeD,
                'A' => $decodeA,
                'FBP' => $decodeFBP,
                'folgeseminar' => $decodefolgeseminar,
                'cat' => $decodecat,
                'kDrei' => $decodekDrei,
                'hKnapp' => $decodehKnapp,
                "ort" => $decodeort,
                "optimice1" => $optimice1,
                "optimice2" => $optimice2,
                "zerti" => $zerti,
                "iszert"  => $iszert
            ));
    }
}

function getFromDbToCookie()
{
    global $wpdb;
    if (session_id() == '' || !isset($_SESSION)) {
        session_start();
    }
    $identifier = session_id();
    $results = $wpdb->get_results("SELECT * FROM forms_sender_receiver WHERE submission = '$identifier'");
    if (!empty($results)) {
        foreach ($results as $result) {
            $toCookieID = $result->id;
            $toCookieSubmission = $result->submission;
            $toCookieS = $result->S;
            $toCookieT = $result->T;
            $toCookieG = $result->G;
            $toCookieI = $result->I;
            $toCookieD = $result->D;
            $toCookieA = $result->A;
            $toCookieFBP = $result->FBP;
            $toCookiefolgeseminar = $result->folgeseminar;
            $toCookiecat = $result->cat;
            $toCookiekDrei = $result->kDrei;
            $toCookiehKnapp = $result->hKnapp;
            $toCookieort = $result->ort;
            $toCookiezerti = $result->zerti;
            $toCookieoptimice1 = $result->optimice1;
            $toCookieoptimice2 = $result->optimice2;

        }

        $buildCookie = array(
            "ID" => $toCookieID,
            "Submission" => $toCookieSubmission,
            "S" => $toCookieS,
            "T" => $toCookieT,
            "G" => $toCookieG,
            "I" => $toCookieI,
            "D" => $toCookieD,
            "A" => $toCookieA,
            "FBP" => $toCookieFBP,
            "folgeseminar" => $toCookiefolgeseminar,
            "cat" => $toCookiecat,
            "kDrei" => $toCookiekDrei,
            "hKnapp" => $toCookiehKnapp,
            "ort" => $toCookieort,
            "zerti" => $toCookiezerti,
            "optimice1" => $toCookieoptimice1,
            "optimice2" => $toCookieoptimice2,
        );
        $json = json_encode($buildCookie);
       setcookie('booking', $json, time() + 3600, '/', "buchungen.gfu.net");
    }

    $decodeA = mb_convert_encoding($_GET['A'], "UTF-8", "Windows-1252");
    if ($decodeA === "Reservieren"){
        header("Location:https://buchungen.gfu.net/?utm_expid=$toCookieoptimice1&utm_referrer=$toCookieoptimice2#vormerken");
    } else {
        header("Location:https://buchungen.gfu.net?utm_expid=$toCookieoptimice1&utm_referrer=$toCookieoptimice2");
    }
}

checkCookie();
