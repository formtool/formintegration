<?php
require_once('../../../wp-load.php');

class finalizerOperations
{

    public function __construct()
    {
        $this->run();
    }

    private function run()
    {
        $this->checkCookies();

    }

    private function checkCookies()
    {
        if (session_id() == '' || !isset($_SESSION)) {
            session_start();
        };
        global $wpdb;
        $identifier = session_id();
        global $wpdb;
        if (isset($_COOKIE['cf7msm_posted_data'])) {
            $this->checkBookingReserve();
            $this->finalizeEntry();
            $this->writeSubmitted();
            $this->writeExpiredDb();
            $this->sentExpiredMail();
            $this->removeCookies();
            $this->deleteSubmittedExpired();
            $this->deleteTemporaryRecords();
            $this->souperMailsender();
        } else {
            if (isset($_COOKIE['booking-finalized'])) {
               header("location: https://www.gfu.net/bereitsgebucht.html");
                exit;
            }
        }
    }

    private function checkBookingReserve()
    {
        if (session_id() == '' || !isset($_SESSION)) {
            session_start();
        };
        global $wpdb;
        $identifier = session_id();
        $whatisit = $wpdb->get_results("SELECT A FROM forms_sender_receiver WHERE submission='$identifier'");
        if (isset($_COOKIE['reserve'])) {
            $wpdb->update('forms_sender_receiver',
                array(
                    "switched" => '1',
                ),
                array(
                    'submission' => $identifier,
                )
            );
        }
    }

    private function finalizeEntry()
    {

        if (session_id() == '' || !isset($_SESSION)) {
            session_start();
        };

        $submission = session_id();
        global $wpdb;
        $wpdb->update('forms_sender_receiver',
            array(
                "finalized" => '1',
            ),
            array(
                'submission' => $submission,
            )
        );

        $wpdb->update('forms_sender_data',
            array(
                "finalized" => '1',
            ),
            array(
                'session' => $submission,
            )
        );
    }

    private function writeSubmitted()
    {
        //header("Access-Control-Allow-Origin: myappdomain.com");
        if (session_id() == '' || !isset($_SESSION)) {
            session_start();
        };
        global $wpdb;
        $identifier = session_id();
        $checkresults = $wpdb->get_results("SELECT `session` FROM forms_sender_submitted WHERE `session` = '$identifier'");
        $rows = $wpdb->get_results("SELECT * FROM forms_sender_receiver AS p JOIN forms_sender_data AS r ON r.session = p.submission WHERE r.session = '$identifier' and p.submission='$identifier'");
        foreach ($rows as $row) {
            if (!empty($checkresults)) {
                $wpdb->update('forms_sender_submitted',
                        array(
                        'idsubmitted' => '',
                        'session' => $row->submission,
                        'ipaddress' => $row->ipaddress,
                        'browserinfo' => $row->browserinfo,
                        'cookie' => $row->cookie,
                        'referrer' => $row->referrer,
                        'urlraw' => $row->urlraw,
                        'S' => $row->S,
                        'T' => $row->T,
                        'G' => $row->G,
                        'I' => $row->I,
                        'D' => $row->D,
                        'A' => $row->A,
                        'FBP' => $row->FBP,
                        'folgeseminar' => $row->folgeseminar,
                        'cat' => $row->cat,
                        'kDrei' => $row->kDrei,
                        'hKnapp' => $row->hKnapp,
                        'ort' => $row->ort,
                        'zerti' => $row->zerti,
                        'googleid' => $row->goggleid,
                        'firm' => $row->firm,
                        'str' => $row->str,
                        'plz' => $row->plz,
                        'firmort' => $row->firmort,
                        'ap_anrede' => $row->ap_anrede,
                        'ap_vorname' => $row->ap_vorname,
                        'ap_nachname' => $row->ap_nachname,
                        'telefon' => $row->telefon,
                        'ap_email' => $row->ap_email,
                        'altbill' => $row->altbill,
                        'rechnung_firma' => $row->rechnung_firma,
                        'rechnung_str' => $row->rechnung_str,
                        'rechnung_plz' => $row->rechnung_plz,
                        'rechnung_ort' => $row->rechnung_ort,
                        'teilnehmer_anrede' => $row->teilnehmer_anrede,
                        'teilnehmer_vorname' => $row->teilnehmer_vorname,
                        'teilnehmer_nachname' => $row->teilnehmer_nachname,
                        'teilnehmer_email' => $row->teilnehmer_email,
                        'teilnehmer_mobile' => $row->Handy_Nr,
                        'tn2_anrede' => $row->tn2_anrede,
                        'tn2_vorname' => $row->tn2_vorname,
                        'tn2_nachname' => $row->tn2_nachname,
                        'tn2_email' => $row->tn2_email,
                        'tn2_mobile' => $row->tn2_Handy_Nr,
                        'tn3_anrede' => $row->tn3_anrede,
                        'tn3_vorname' => $row->tn3_vorname,
                        'tn3_nachname' => $row->tn3_nachname,
                        'tn3_email' => $row->tn3_email,
                        'tn3_mobile' => $row->tn3_Handy_Nr,
                        'parkplatz' => $row->parkplatz,
                        'shuttle' => $row->shuttle,
                        'hotelreservierung' => $row->hotelreservierung,
                        'hotel' => $row->hotel,
                        'anreise' => $row->anreise,
                        'abreise' => $row->abreise,
                        'sonstiges1' => $row->sonstiges1,
                        'switched' => $row->switched
                    ),
                    array(
                        'session' => $row->submission,
                    )
                );
            } else {
                $wpdb->insert('forms_sender_submitted',
                    array(
                        'idsubmitted' => '',
                        'session' => $row->submission,
                        'ipaddress' => $row->ipaddress,
                        'browserinfo' => $row->browserinfo,
                        'cookie' => $row->cookie,
                        'referrer' => $row->referrer,
                        'urlraw' => $row->urlraw,
                        'S' => $row->S,
                        'T' => $row->T,
                        'G' => $row->G,
                        'I' => $row->I,
                        'D' => $row->D,
                        'A' => $row->A,
                        'FBP' => $row->FBP,
                        'folgeseminar' => $row->folgeseminar,
                        'cat' => $row->cat,
                        'kDrei' => $row->kDrei,
                        'hKnapp' => $row->hKnapp,
                        'ort' => $row->ort,
                        'zerti' => $row->zerti,
                        'googleid' => $row->goggleid,
                        'firm' => $row->firm,
                        'str' => $row->str,
                        'plz' => $row->plz,
                        'firmort' => $row->firmort,
                        'ap_anrede' => $row->ap_anrede,
                        'ap_vorname' => $row->ap_vorname,
                        'ap_nachname' => $row->ap_nachname,
                        'telefon' => $row->telefon,
                        'ap_email' => $row->ap_email,
                        'altbill' => $row->altbill,
                        'rechnung_firma' => $row->rechnung_firma,
                        'rechnung_str' => $row->rechnung_str,
                        'rechnung_plz' => $row->rechnung_plz,
                        'rechnung_ort' => $row->rechnung_ort,
                        'teilnehmer_anrede' => $row->teilnehmer_anrede,
                        'teilnehmer_vorname' => $row->teilnehmer_vorname,
                        'teilnehmer_nachname' => $row->teilnehmer_nachname,
                        'teilnehmer_email' => $row->teilnehmer_email,
                        'teilnehmer_mobile' => $row->Handy_Nr,
                        'tn2_anrede' => $row->tn2_anrede,
                        'tn2_vorname' => $row->tn2_vorname,
                        'tn2_nachname' => $row->tn2_nachname,
                        'tn2_email' => $row->tn2_email,
                        'tn2_mobile' => $row->tn2_Handy_Nr,
                        'tn3_anrede' => $row->tn3_anrede,
                        'tn3_vorname' => $row->tn3_vorname,
                        'tn3_nachname' => $row->tn3_nachname,
                        'tn3_email' => $row->tn3_email,
                        'tn3_mobile' => $row->tn3_Handy_Nr,
                        'parkplatz' => $row->parkplatz,
                        'shuttle' => $row->shuttle,
                        'hotelreservierung' => $row->hotelreservierung,
                        'hotel' => $row->hotel,
                        'anreise' => $row->anreise,
                        'abreise' => $row->abreise,
                        'sonstiges1' => $row->sonstiges1,
                        'switched' => $row->switched
                    ));
            }
        }
    }

    private function writeExpiredDb()
    {
        global $wpdb;
        $rows = $wpdb->get_results("SELECT * FROM forms_sender_receiver AS fsr JOIN forms_sender_data AS fsd WHERE fsr.created < (NOW()- INTERVAL 240 MINUTE) and fsd.datacreated < (NOW()- INTERVAL 240 MINUTE) and fsr.submission = fsd.session;");
        foreach ($rows as $row) {
            $wpdb->insert('forms_sender_submitted',
                array(
                    'idsubmitted' => '',
                    'session' => $row->submission,
                    'ipaddress' => $row->ipaddress,
                    'browserinfo' => $row->browserinfo,
                    'cookie' => $row->cookie,
                    'referrer' => $row->referrer,
                    'urlraw' => $row->urlraw,
                    'S' => $row->S,
                    'T' => $row->T,
                    'G' => $row->G,
                    'I' => $row->I,
                    'D' => $row->D,
                    'A' => $row->A,
                    'FBP' => $row->FBP,
                    'folgeseminar' => $row->folgeseminar,
                    'cat' => $row->cat,
                    'kDrei' => $row->kDrei,
                    'hKnapp' => $row->hKnapp,
                    'ort' => $row->ort,
                    'zerti' => $row->zerti,
                    'googleid' => $row->goggleid,
                    'firm' => $row->firm,
                    'str' => $row->str,
                    'plz' => $row->plz,
                    'firmort' => $row->firmort,
                    'ap_anrede' => $row->ap_anrede,
                    'ap_vorname' => $row->ap_vorname,
                    'ap_nachname' => $row->ap_nachname,
                    'telefon' => $row->telefon,
                    'ap_email' => $row->ap_email,
                    'altbill' => $row->altbill,
                    'rechnung_firma' => $row->rechnung_firma,
                    'rechnung_str' => $row->rechnung_str,
                    'rechnung_plz' => $row->rechnung_plz,
                    'rechnung_ort' => $row->rechnung_ort,
                    'teilnehmer_anrede' => $row->teilnehmer_anrede,
                    'teilnehmer_vorname' => $row->teilnehmer_vorname,
                    'teilnehmer_nachname' => $row->teilnehmer_nachname,
                    'teilnehmer_email' => $row->teilnehmer_email,
                    'teilnehmer_mobile' => $row->Handy_Nr,
                    'tn2_anrede' => $row->tn2_anrede,
                    'tn2_vorname' => $row->tn2_vorname,
                    'tn2_nachname' => $row->tn2_nachname,
                    'tn2_email' => $row->tn2_email,
                    'tn2_mobile' => $row->tn2_Handy_Nr,
                    'tn3_anrede' => $row->tn3_anrede,
                    'tn3_vorname' => $row->tn3_vorname,
                    'tn3_nachname' => $row->tn3_nachname,
                    'tn3_email' => $row->tn3_email,
                    'tn3_mobile' => $row->tn3_Handy_Nr,
                    'parkplatz' => $row->parkplatz,
                    'shuttle' => $row->shuttle,
                    'hotelreservierung' => $row->hotelreservierung,
                    'hotel' => $row->hotel,
                    'anreise' => $row->anreise,
                    'abreise' => $row->abreise,
                    'sonstiges1' => $row->sonstiges1,
                    'switched' => $row->switched,
                    'expired' => '1',
                ));
        }
    }

    private function sentExpiredMail(){
                        global $wpdb;
                        $maildata = $wpdb->get_results("SELECT * FROM forms_sender_submitted WHERE expiredmail = '0'");
                        $senddata = [];
                        foreach ($maildata as $maildate) {
                            $senddata = $maildate;
                         }


$mailcontent = <<<HTML
Session:
                      Ip: {$senddata->ipaddress}
                      Original Url:  {$senddata->urlraw}
                      Browser: {$senddata->browserinfo}
---------------------------------------------------------------------
Bestellparameter:     
                      S: {$senddata->S}
                      T: {$senddata->T}
                      Beginn des Ausfuellens: {$senddata->submitted}
                      I: {$senddata->I}
                      D: {$senddata->D}
                      A: {$senddata->A}
---------------------------------------------------------------------
Firma:                     
                      Firma: {$senddata->firm} 
                      Straße: {$senddata->str}
                      PLZ:{$senddata->plz}
                      Firmaort: {$senddata->firmort}  
---------------------------------------------------------------------
Rechnungsanschrift:                       
                      Rechnung Firma:{$senddata->rechnung_firma}
                      Rechnung PLZ: {$senddata->rechnung_plz}                     
                      Rechnung Ort: {$senddata->rechnung_ort}
                      Rechnung Str: {$senddata->rechnung_str}
---------------------------------------------------------------------
                       
Ansprechpartner:
                      AP Anreder:  {$senddata->ap_anrede}
                      AP Name: {$senddata->ap_nachname}
                      AP Vorname: {$senddata->ap_vorname}
                      AP Telefon: {$senddata->telefon}
                      AP Mail: {$senddata->ap_email}
---------------------------------------------------------------------
Teilnehmer:                        
                      Teilnehmer Anrede: {$senddata->teilnehmer_anrede}
                      Teilnehmer Nachname: {$senddata->teilnehmer_nachname}
                      Teilnehmer Vorname: {$senddata->teilnehmer_vorname}
                      Teilnehmer Mail: {$senddata->teilnehmer_email}
                      Teilnehmer Mobile: {$senddata->teilnehmer_mobile}
---------------------------------------------------------------------
                      
Teilnehmer2:                
                      Teilnehmer2 Anrede: {$senddata->tn2_anrede}
                      Teilnehmer2 Nachname:{$senddata->tn2_nachname}
                      Teilnehmer2 Vorname: {$senddata->tn2_vorname}                       
                      Teilnehmer2 Mobile: {$senddata->tn2_mobile}
                      Teilnehmer2 Mail: {$senddata->tn2_email}
---------------------------------------------------------------------
Teilnehmer3:                  
                      Teilnehmer3 Anrede:  {$senddata->tn3_anrede}
                      Teilnehmer3 Nachname: {$senddata->tn3_nachname}
                      Teilnehmer3 Vorname: {$senddata->tn3_vorname}
                      Teilnehmer3 Mail: {$senddata->tn3_email}
                      Teilnehmer3 Mobile: {$senddata->tn3_mobile}
---------------------------------------------------------------------
Hotel 
                      Hotelreservierung  {$senddata->hotelreservierung}
                      Hotel  {$senddata->hotel}                     
                      Anreise  {$senddata->anreise}
                      Abreise {$senddata->abreise}
---------------------------------------------------------------------
                        
Service:                        
                      Parkplatz: {$senddata->parkplatz}
                      Shuttle: {$senddata->shuttle}
---------------------------------------------------------------------
Anmerkungen:
                        Bemerkungen: {$senddata->sonstiges1}
---------------------------------------------------------------------
HTML;

        $empfaenger = "beuse@gfu.net";
        $betreff = "Nicht beendeter Bestellvorgang";
        $from = "From: GFU Bestellungen <lars@gfu.net>";
                    //mail($empfaenger, $betreff, $mailcontent, $from);

                       $wpdb->update('forms_sender_submitted',
                            array(
                                "expiredmail" => '1',
                            ),
                            array(
                                'expiredmail' => '0',
                            )
                        );

    }

    private function deleteSubmittedExpired(){
        global $wpdb;
        $entrys = $wpdb->query("Delete fsr.*, fsd.* FROM forms_sender_data as fsd, forms_sender_receiver as fsr, forms_sender_submitted as fss WHERE fsr.submission = fsd.session AND fsr.submission = fss.session and fss.expired = 1");
        foreach ($entrys as $entry){


        //    $fsrrow = $entry->submission;
        //   $fsdrow = $entry->session;
        // print_r($fsrrow);
        //    print_r($fsdrow);

          //$wpdb->delete( 'forms_sender_receiver', array( 'submission' => $fsrrow ) );
         // $wpdb->delete( 'forms_sender_data', array( 'submission' => $fsdrow ) );

        }
    }


    private function deleteTemporaryRecords()
    {

        if (session_id() == '' || !isset($_SESSION)) {
            session_start();
        };
        global $wpdb;
        $identifier = session_id();
        $checkresults = $wpdb->get_results("SELECT `session` FROM forms_sender_submitted WHERE `session` = '$identifier'");
        if (!empty ($checkresults)) {
            $wpdb->query("DELETE FROM forms_sender_receiver WHERE submission = '$identifier'");
            $wpdb->query("DELETE FROM forms_sender_data WHERE `session` = '$identifier'");
        }
    }

    private function removeCookies()
    {
        if (isset($_SERVER['HTTP_COOKIE'])) {
            $cookies = explode(';', $_SERVER['HTTP_COOKIE']);
            foreach ($cookies as $cookie) {
                $parts = explode('=', $cookie);
                $name = trim($parts[0]);
                setcookie($name, '', time() - 1000, '/');
                setcookie($name, '', time() - 1000, '/', "buchungen.gfu.net");
            }
        }
    }

    private function souperMailsender()
    {

        if (session_id() == '' || !isset($_SESSION)) {
            session_start();
        };
        global $wpdb;
        $identifier = session_id();
        $rows = $wpdb->get_results("
                                        SELECT * FROM forms_sender_submitted WHERE `session` = '$identifier' 
                                  ");
        $livedata = [];
        foreach ($rows as $row) {
            $livedata = $row;
        }

        $html = <<<HTML
    <form id="f" name="f" action="https://www.gfu.net/cgi-bin/soupermail_test.pl" method="post">
       <input name="SoupermailConf" type="hidden" value="/conf/anmeldung_test.con"/>
       <input type="hidden" name="Email" value="soupermail@gfu.net">
       <input type="hidden" name="semtitel" value="{$livedata->S}">
       <input type="hidden" name="semcat" value="{$livedata->cat}">
       <input type="hidden" name="semtermin" value="{$livedata->T}">
       <input type="hidden" name="semdauer" value="{$livedata->D}">
       <input type="hidden" name="sempreis" class="p" value="{$livedata->G}">
       <input type="hidden" name="semid" value="{$livedata->I}">
       <input type="hidden" name="semort" value="{$livedata->ort}">
       <input type="hidden" name="folgeseminar" value="{$livedata->folgeseminar}">
       <input type="hidden" name="folgebuchung" value="{$livedata->FBP}">
       <input type="hidden" name="kDrei" value="{$livedata->kDrei}">
       <input type="hidden" name="zerti" value="{$livedata->zerti}">
       <input name="teilnehmer_anrede" type="hidden" value="{$livedata->teilnehmer_anrede}"/>
       <input name="teilnehmer_vorname" type="hidden" value="{$livedata->teilnehmer_vorname}"/>
       <input name="teilnehmer_nachname" type="hidden" value="{$livedata->teilnehmer_nachname}"/>
       <input name="email" type="hidden" value="{$livedata->teilnehmer_email}"/>
       <input name="Handy_Nr" type="hidden" value="{$livedata->teilnehmer_mobile }"/>
       <input name="tn2_anrede" type="hidden" value="{$livedata->tn2_anrede}"/>
       <input name="tn2_vorname" type="hidden" value="{$livedata->tn2_vorname}"/>
       <input name="tn2_nachname" type="hidden" value="{$livedata->tn2_nachname}"/>
       <input name="tn2_email" type="hidden" value="{$livedata->tn2_email}"/>
       <input name="tn2_Handy_Nr" type="hidden" value="{$livedata->teilnehmer_mobile}"/>
       <input name="tn3_anrede" type="hidden" value="{$livedata->tn3_anrede}"/>
       <input name="tn3_vorname" type="hidden" value="{$livedata->tn3_vorname}"/>
       <input name="tn3_nachname" type="hidden" value="{$livedata->tn3_nachname}"/>
       <input name="tn3_email" type="hidden" value="{$livedata->tn3_email}"/>
       <input name="tn3_Handy_Nr" type="hidden" value="{$livedata->tn3_mobile}"/>
       <input name="firma" type="hidden" value="{$livedata->firm}"/>
       <input name="str" type="hidden" value="{$livedata->str}"/>
       <input name="plz" type="hidden" value="{$livedata->plz}"/>
       <input name="ort" type="hidden" value="{$livedata->firmort}"/>
       <input name="ap_anrede" type="hidden" value="{$livedata->ap_anrede}"/>
       <input name="ap_vorname" type="hidden" value="{$livedata->ap_vorname}"/>
       <input name="ap_nachname" type="hidden" value="{$livedata->ap_nachname}"/>
       <input name="telefon" type="hidden" value="{$livedata->telefon}"/>
       <input name="ap_email" type="hidden" value="{$livedata->ap_email}"/>
       <input name="alternativerechnung" type="hidden" value="{$livedata->altbill}"/>
       <input name="rechnung_firma" type="hidden" value="{$livedata->rechnung_firma}"/>
       <input name="rechnung_str" type="hidden" value="{$livedata->rechnung_str}"/>
       <input name="rechnung_plz" type="hidden" value="{$livedata->rechnung_plz}"/>
       <input name="rechnung_ort" type="hidden" value="{$livedata->rechnung_ort}"/>
       <input name="parkplatz" type="hidden" value="{$livedata->parkplatz}"/>
       <input name="shuttle" type="hidden" value="{$livedata->shuttle}"/>
       <input name="hotelreservierung" type="hidden" value="{$livedata->hotelreservierung}"/>
       <input name="hotel" type="hidden" value="{$livedata->hotel}"/>
       <input name="anreise" type="hidden" value="{$livedata->anreise}"/>
       <input name="abreise" type="hidden" value="{$livedata->abreise}"/>
       <input name="sonstiges1" type="hidden" value="{$livedata->sonstiges1}"/>
        <script type="text/javascript">
      document.getElementById('f').submit();
        </script>
    </form>
HTML;

        $html2 = <<<HTML
    <form id="fRes" name="fRes" action="https://www.gfu.net/cgi-bin/soupermail_test.pl" method="post">
        <input name="SoupermailConf" type="hidden" value="/conf/reservierung2_test.con"/>
        <input type="hidden" name="Email" value="soupermail@gfu.net">
        <input name="semcat" type="hidden" value="{$livedata->cat}">
        <input name="semtitel" type="hidden" value="{$livedata->S}">
        <input name="semtermin" type="hidden" value="{$livedata->T}">
        <input name="semdauer" type="hidden" value="{$livedata->D}">
        <input name="kDrei" type="hidden" value="{$livedata->kDrei}">
        <input name="semid" type="hidden" value="{$livedata->I}">
        <input name="semort" type="hidden" value="{$livedata->ort}">
        <input name="sempreis" type="hidden" class="p" value="{$livedata->G}">
        <input name="res_firma" type="hidden" value="{$livedata->firm}"/>
        <input name="res_ap_anrede" type="hidden" value="{$livedata->ap_anrede}"/>
        <input name="res_ap_vorname" type="hidden" value="{$livedata->ap_vorname}"/>
        <input name="res_ap_nachname" type="hidden" value="{$livedata->ap_nachname}"/>
        <input name="res_telefon" type="hidden" value="{$livedata->telefon}"/>
        <input name="res_email" type="hidden" value="{$livedata->ap_email}"/>
        <input name="res_telefon" type="hidden" value="{$livedata->telefon}"/>
        <input name="res_teilnehmer_anrede" type="hidden" value="{$livedata->teilnehmer_anrede}"/>
        <input name="res_teilnehmer_vorname" type="hidden" value="{$livedata->teilnehmer_vorname}"/>
        <input name="res_teilnehmer_nachname" type="hidden" value="{$livedata->teilnehmer_nachname}"/>
        <input name="res_teilnehmer2_anrede" type="hidden" value="{$livedata->tn2_anrede}"/>
        <input name="res_teilnehmer2_vorname" type="hidden" value="{$livedata->tn2_vorname}"/>
        <input name="res_teilnehmer2_nachname" type="hidden" value="{$livedata->tn2_nachname}"/>
        <input name="res_teilnehmer3_anrede" type="hidden" value="{$livedata->tn3_anrede}"/>
        <input name="res_teilnehmer3_vorname" type="hidden" value="{$livedata->tn3_vorname}"/>
        <input name="res_teilnehmer3_nachname" type="hidden" value="{$livedata->tn3_nachname}"/>
    </form>
        <script type="text/javascript">
    document.getElementById('fRes').submit();
        </script>
HTML;

        if (session_id() == '' || !isset($_SESSION)) {
            session_start();
        };

        global $wpdb;
        $identifier = session_id();

        if (isset($_COOKIE['reserve'])) {
            setcookie('booking-finalized', '1', time() + 3600, '/', "buchungen.gfu.net");
            echo $html2;
        } else {
            setcookie('booking-finalized', '1', time() + 3600, '/', "buchungen.gfu.net");
            echo $html;
        }
    }
}

$obj = new finalizerOperations();
