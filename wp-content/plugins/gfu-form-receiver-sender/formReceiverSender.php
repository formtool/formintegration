<?php
/*
Plugin Name: Formtool Receiver Sender
Plugin URI: http://gfu.net
Description: Receiverframework
Author: Lars H. Beuse
Author URI: http://swashmark.com

Version: BETA 0.2

License: GNU General Public License v2.0 (or later)
License URI: http://www.opensource.org/licenses/gpl-license.php
*/

if (!class_exists('formtoolSenderReceiver')) {
    add_action('init', array('formtoolSenderReceiver', 'go'));

    class formtoolSenderReceiver
    {
        private $var_sTextdomain;

        public static function go()
        {
            $class = __CLASS__;
            new $class;
        }

        private function __construct()
        {
            $this->run($toCookie);
            add_shortcode('Useragent', array($this, 'userAgentSniff'));
            add_shortcode('Ipaddress', array($this, 'userIpSniff'));
            add_shortcode('Referrer', array($this, 'userRefSniff'));
            add_shortcode('Userurl', array($this, 'userUrlraw'));
            add_shortcode('Param', array($this, 'urlParams'));
            add_shortcode('Conditional', array($this, 'conditionalSc'));
            add_shortcode('Jsonpostdata', array($this, 'readMultistepData'));
            add_shortcode('Analyticsuid', array($this, 'getGAUID'));
            add_shortcode('Writevalues', array($this, 'writeFormData'));
            add_filter('page_template', array($this, 'statistics_page_template'));

        }

        private function run($atts = array())
        {
            $this->statistics_page_template($page_template);
            $this->writeFormDataDb();
            $this->userAgentSniff();
            $this->userIpSniff();
            $this->urlParams($atts = array());
            $this->userUrlraw($atts = array());
            $this->conditionalSc($atts = array());
            $this->readMultistepData();
            $this->writeFormData($atts = array());
        }

        public function statistics_page_template($page_template)
        {
            if (is_page('statistics')) {
                $page_template = dirname(__FILE__) . '/charts/statistics-template.php';
            }
            return $page_template;
        }


        public function readMultistepData()
        {
            $data = $_COOKIE['cf7msm_posted_data'];
            $dataclean = stripslashes($data);
            return $dataclean;
        }

        public function userAgentSniff()
        {
            global $wpdb;
            if (session_id() == '' || !isset($_SESSION)) {
                session_start();
            }
            $identifier = session_id();
            $results = $wpdb->get_results("SELECT browserinfo FROM forms_sender_receiver WHERE submission = '$identifier'");
            foreach ($results as $result) {
                return $result->browserinfo;
            }
        }

        public function userIpSniff()
        {
            global $wpdb;
            if (session_id() == '' || !isset($_SESSION)) {
                session_start();
            }
            $identifier = session_id();
            $results = $wpdb->get_results("SELECT ipaddress FROM forms_sender_receiver WHERE submission = '$identifier'");
            foreach ($results as $result) {
                return $result->ipaddress;
            }
        }

        public function userRefSniff()
        {
            global $wpdb;
            if (session_id() == '' || !isset($_SESSION)) {
                session_start();
            }
            $identifier = session_id();
            $results = $wpdb->get_results("SELECT referrer FROM forms_sender_receiver WHERE submission = '$identifier'");
            foreach ($results as $result) {
                return $result->referrer;
            }
        }

        public function userUrlraw($atts = array())
        {
            if (session_id() == '' || !isset($_SESSION)) {
                session_start();
            }

            global $wpdb;
            $identifier = session_id();

            extract(shortcode_atts(array(
                'urlraw' => 'urlraw'
            ), $atts));

            switch ($urlraw) {
                case 'cookie':
                    $sql = $wpdb->get_results("SELECT urlraw FROM forms_sender_receiver WHERE submission = '$identifier'");
                    foreach ($sql as $result) {
                        return $result->urlraw;
                    }
                    break;

                case 'nocookie':
                    $urlraw = $_SERVER['REQUEST_URI'];
                    $url = strstr($urlraw, '?');
                    return $url;
                    break;

                default:
                    $sql = 'no params';
                    break;
            }
        }


        public function urlParams($atts = array())
        {
            if (session_id() == '' || !isset($_SESSION)) {
                session_start();
            }
            global $wpdb;
            $identifier = session_id();
            extract(shortcode_atts(array(
                'params' => 'params'
            ), $atts));
            switch ($params) {
                case 'paramsjson':
                    $sql = $wpdb->get_results("SELECT * FROM forms_sender_receiver WHERE submission = '$identifier'");
                    foreach ($sql as $result) {
                        $jsonify = (stripslashes(json_encode($result)));
                        return $jsonify;
                    }
                    break;
                case 'submission':
                    $sql = $wpdb->get_results("SELECT submission FROM forms_sender_receiver WHERE submission = '$identifier'");
                    foreach ($sql as $result) {
                        return $result->submission;
                    }
                    break;
                case 'S':
                    $sql = $wpdb->get_results("SELECT S FROM forms_sender_receiver WHERE submission = '$identifier'");
                    foreach ($sql as $result) {
                        return $result->S;
                    }
                    break;
                case 'T':
                    $sql = $wpdb->get_results("SELECT T FROM forms_sender_receiver WHERE submission = '$identifier'");
                    foreach ($sql as $result) {
                        return $result->T;
                    }
                    break;
                case 'D':
                    $sql = $wpdb->get_results("SELECT D FROM forms_sender_receiver WHERE submission = '$identifier'");
                    foreach ($sql as $result) {
                        return $result->D;
                    }
                    break;
                case 'G':
                    $sql = $wpdb->get_results("SELECT G FROM forms_sender_receiver WHERE submission = '$identifier'");
                    foreach ($sql as $result) {
                        return $result->G;
                    }
                    break;
                case 'I':
                    $sql = $wpdb->get_results("SELECT I FROM forms_sender_receiver WHERE submission = '$identifier'");
                    foreach ($sql as $result) {
                        return $result->I;
                    }
                    break;
                case 'A':
                    $sql = $wpdb->get_results("SELECT A FROM forms_sender_receiver WHERE submission = '$identifier'");
                    foreach ($sql as $result) {
                        return $result->A;
                    }
                    break;
                case 'FBP':
                    $sql = $wpdb->get_results("SELECT FBP FROM forms_sender_receiver WHERE submission = '$identifier'");
                    foreach ($sql as $result) {
                        return $result->FBP;
                    }
                    break;
                case 'folgeseminar':
                    $sql = $wpdb->get_results("SELECT folgeseminar FROM forms_sender_receiver WHERE submission = '$identifier'");
                    foreach ($sql as $result) {
                        return $result->folgeseminar;
                    }
                    break;
                case 'cat':
                    $sql = $wpdb->get_results("SELECT cat FROM forms_sender_receiver WHERE submission = '$identifier'");
                    foreach ($sql as $result) {
                        return $result->cat;
                    }
                    break;
                case 'kDrei':
                    $sql = $wpdb->get_results("SELECT kDrei FROM forms_sender_receiver WHERE submission = '$identifier'");
                    foreach ($sql as $result) {
                        return $result->kDrei;
                    }
                    break;
                case 'hKnapp':
                    $sql = $wpdb->get_results("SELECT hKnapp FROM forms_sender_receiver WHERE submission = '$identifier'");
                    foreach ($sql as $result) {
                        return $result->hKnapp;
                    }
                    break;
                case 'ort':
                    $sql = $wpdb->get_results("SELECT ort FROM forms_sender_receiver WHERE submission = '$identifier'");
                    foreach ($sql as $result) {
                        return $result->ort;
                    }
                    break;
                case 'zerti':
                    $sql = $wpdb->get_results("SELECT zerti FROM forms_sender_receiver WHERE submission = '$identifier'");
                    foreach ($sql as $result) {
                        return $result->zerti;
                    }
                    break;
                default:
                    $sql = 'no params';
                    break;
            }
        }

        public function conditionalSc($atts = array(), $content = null)
        {
            if (session_id() == '' || !isset($_SESSION)) {
                session_start();
            }
            global $wpdb;
            $identifier = session_id();
            extract(shortcode_atts(array(
                'value' => 'value'
            ), $atts));

            switch ($value) {
                case 'hknapp':
                    $sql = $wpdb->get_results("SELECT hKnapp FROM forms_sender_receiver WHERE submission = '$identifier'");
                    foreach ($sql as $result) {
                        if ($result->hKnapp == 0) {
                            return;
                        }
                        if ($result->hKnapp == 1) {
                            return $content;
                        }
                    }
                case 'kDrei':
                    $sql = $wpdb->get_results("SELECT kDrei FROM forms_sender_receiver WHERE submission = '$identifier'");
                    foreach ($sql as $result) {
                        if ($result->hKnapp == 0) {
                            return;
                        }
                        if ($result->hKnapp == 1) {
                            return $content;
                        }
                    }
            }
        }


        public function writeFormDataDb()
        {
            global $wpdb;
            if (session_id() == '' || !isset($_SESSION)) {
                session_start();
            }
            $identifier = session_id();
            $checkresults = $wpdb->get_results("SELECT session FROM forms_sender_data WHERE session = '$identifier'");

            $dataraw = (stripslashes($_COOKIE['cf7msm_posted_data']));

            $dataarray = json_decode($dataraw, true);


            $alternatebilling = array();
            foreach ($dataarray["rechnungsanschrift"] as $bill) {
                if (isset ($bill)) {
                    $alternatebilling = 1;
                } else {
                    $alternatebilling = 0;
                }
            }

            $shuttlerservice = array();
            foreach ($dataarray["shuttle"] as $shuttle) {
                if (isset ($shuttle)) {
                    $shuttlerservice = 1;
                }
            }

            $parkplatzservice = array();
            foreach ($dataarray["parkplatz"] as $parkplatz) {
                if (isset ($parkplatz)) {
                    $parkplatzservice = 1;
                }
            }

            $hotelservice = array();
            foreach ($dataarray["hotelreservierung"] as $hotelreservierung) {
                if (isset ($hotelreservierung)) {
                    $hotelservice = 1;
                }
            }

            if (isset($_COOKIE['cf7msm_posted_data'])) {
                if (!empty ($checkresults)) {
                    $wpdb->update('forms_sender_data',
                        array(
                            'firm' => $dataarray['firma'],
                            'str' => $dataarray["str"],
                            'plz' => $dataarray["plz"],
                            'firmort' => $dataarray["ort"],
                            'ap_anrede' => $dataarray["ap_anrede"],
                            'ap_vorname' => $dataarray["ap_vorname"],
                            'ap_nachname' => $dataarray["ap_nachname"],
                            'telefon' => $dataarray["telefon"],
                            'ap_email' => $dataarray["ap_email"],
                            'altbill' => $alternatebilling,
                            'rechnung_firma' => $dataarray["rechnung_firma"],
                            'rechnung_str' => $dataarray["rechnung_str"],
                            'rechnung_plz' => $dataarray["rechnung_plz"],
                            'rechnung_ort' => $dataarray["rechnung_ort"],
                            'teilnehmer_anrede' => $dataarray["teilnehmer_anrede"],
                            'teilnehmer_vorname' => $dataarray["teilnehmer_vorname"],
                            'teilnehmer_nachname' => $dataarray["teilnehmer_nachname"],
                            'teilnehmer_email' => $dataarray["email"],
                            'Handy_Nr' => $dataarray["Handy_Nr"],
                            'tn2_anrede' => $dataarray["tn2_anrede"],
                            'tn2_vorname' => $dataarray["tn2_vorname"],
                            'tn2_nachname' => $dataarray["tn2_nachname"],
                            'tn2_email' => $dataarray["tn2_email"],
                            'tn2_Handy_Nr' => $dataarray["tn2_Handy_Nr"],
                            'tn3_anrede' => $dataarray["tn3_anrede"],
                            'tn3_vorname' => $dataarray["tn3_vorname"],
                            'tn3_nachname' => $dataarray["tn3_nachname"],
                            'tn3_email' => $dataarray["tn3_email"],
                            'tn3_Handy_Nr' => $dataarray["tn3_Handy_Nr"],
                            'parkplatz' => $parkplatzservice,
                            'shuttle' => $shuttlerservice,
                            'hotelreservierung' => $hotelservice,
                            'hotel' => $dataarray["hotel"],
                            'anreise' => $dataarray["anreise"],
                            'abreise' => $dataarray["abreise"],
                            'sonstiges1' => $dataarray["sonstiges1"]

                        ),
                        array(
                            'session' => $identifier,
                        )
                    );
                } else {
                    $wpdb->insert('forms_sender_data',
                        array(
                            'session' => $identifier,
                            'firm' => $dataarray['firma'],
                            'str' => $dataarray["str"],
                            'plz' => $dataarray["plz"],
                            'firmort' => $dataarray["ort"],
                            'ap_anrede' => $dataarray["ap_anrede"],
                            'ap_vorname' => $dataarray["ap_vorname"],
                            'ap_nachname' => $dataarray["ap_nachname"],
                            'telefon' => $dataarray["telefon"],
                            'ap_email' => $dataarray["ap_email"],
                            'altbill' => $alternatebilling,
                            'rechnung_firma' => $dataarray["rechnung_firma"],
                            'rechnung_str' => $dataarray["rechnung_str"],
                            'rechnung_plz' => $dataarray["rechnung_plz"],
                            'rechnung_ort' => $dataarray["rechnung_ort"],
                            'teilnehmer_anrede' => $dataarray["teilnehmer_anrede"],
                            'teilnehmer_vorname' => $dataarray["teilnehmer_vorname"],
                            'teilnehmer_nachname' => $dataarray["teilnehmer_nachname"],
                            'teilnehmer_email' => $dataarray["email"],
                            'Handy_Nr' => $dataarray["Handy_Nr"],
                            'tn2_anrede' => $dataarray["tn2_anrede"],
                            'tn2_vorname' => $dataarray["tn2_vorname"],
                            'tn2_nachname' => $dataarray["tn2_nachname"],
                            'tn2_email' => $dataarray["tn2_email"],
                            'tn2_Handy_Nr' => $dataarray["tn2_Handy_Nr"],
                            'tn3_anrede' => $dataarray["tn3_anrede"],
                            'tn3_vorname' => $dataarray["tn3_vorname"],
                            'tn3_nachname' => $dataarray["tn3_nachname"],
                            'tn3_email' => $dataarray["tn3_email"],
                            'tn3_Handy_Nr' => $dataarray["tn3_Handy_Nr"],
                            'parkplatz' => $parkplatzservice,
                            'shuttle' => $shuttlerservice,
                            'hotelreservierung' => $hotelservice,
                            'hotel' => $dataarray["hotel"],
                            'anreise' => $dataarray["anreise"],
                            'abreise' => $dataarray["abreise"],
                            'sonstiges1' => $dataarray["sonstiges1"],
                            'expired' => '0',

                        ));
                }
            }
        }

        public function writeFormData($atts = array())
        {
            global $wpdb;
            if (session_id() == '' || !isset($_SESSION)) {
                session_start();
            }
            $identifier = session_id();

            extract(shortcode_atts(array(
                'cookievalue' => 'cookievalue'
            ), $atts));
            $cookievalues = $_COOKIE["cf7msm_posted_data"];
            $cookievaluesrender = (stripslashes($cookievalues));
            $cookievalueswriteable = array(json_decode($cookievaluesrender));


            switch ($cookievalue) {


                case 'firm':
                    $sql = $wpdb->get_results("SELECT firm FROM forms_sender_data WHERE `session` = '$identifier'");
                    foreach ($sql as $result) {
                        return $result->firm;
                    }
                    break;
                case 'str':
                    $sql = $wpdb->get_results("SELECT str FROM forms_sender_data WHERE `session` = '$identifier'");
                    foreach ($sql as $result) {
                        return $result->str;
                    }
                    break;
                case 'plz':
                    $sql = $wpdb->get_results("SELECT plz FROM forms_sender_data WHERE `session` = '$identifier'");
                    foreach ($sql as $result) {
                        return $result->plz;
                    }
                    break;
                case 'ort':
                    $sql = $wpdb->get_results("SELECT firmort FROM forms_sender_data WHERE `session` = '$identifier'");
                    foreach ($sql as $result) {
                        return $result->firmort;
                    }
                    break;
                case 'ap_anrede':
                    $sql = $wpdb->get_results("SELECT ap_anrede FROM forms_sender_data WHERE `session` = '$identifier'");
                    foreach ($sql as $result) {
                        return $result->ap_anrede;
                    }
                    break;
                case 'ap_vorname':
                    $sql = $wpdb->get_results("SELECT ap_vorname FROM forms_sender_data WHERE `session` = '$identifier'");
                    foreach ($sql as $result) {
                        return $result->ap_vorname;
                    }
                    break;
                case 'ap_nachname':
                    $sql = $wpdb->get_results("SELECT ap_nachname FROM forms_sender_data WHERE `session` = '$identifier'");
                    foreach ($sql as $result) {
                        return $result->ap_nachname;
                    }
                    break;
                case 'telefon':
                    $sql = $wpdb->get_results("SELECT telefon FROM forms_sender_data WHERE `session` = '$identifier'");
                    foreach ($sql as $result) {
                        return $result->telefon;
                    }
                    break;
                case 'ap_email':
                    $sql = $wpdb->get_results("SELECT ap_email FROM forms_sender_data WHERE `session` = '$identifier'");
                    foreach ($sql as $result) {
                        return $result->ap_email;
                    }
                    break;
                case 'altbill':
                    $sql = $wpdb->get_results("SELECT altbill FROM forms_sender_data WHERE `session` = '$identifier'");
                    foreach ($sql as $result) {
                        return $result->altbill;
                    }
                    break;
                case 'rechnung_firma':
                    $sql = $wpdb->get_results("SELECT rechnung_firma FROM forms_sender_data WHERE `session` = '$identifier'");
                    foreach ($sql as $result) {
                        return $result->rechnung_firma;
                    }
                    break;

                case 'rechnung_str':
                    $sql = $wpdb->get_results("SELECT rechnung_str FROM forms_sender_data WHERE `session` = '$identifier'");
                    foreach ($sql as $result) {
                        return $result->rechnung_str;
                    }
                    break;

                case 'rechnung_plz':
                    $sql = $wpdb->get_results("SELECT rechnung_plz FROM forms_sender_data WHERE `session` = '$identifier'");
                    foreach ($sql as $result) {
                        return $result->rechnung_plz;
                    }
                    break;

                case 'rechnung_ort':
                    $sql = $wpdb->get_results("SELECT rechnung_ort FROM forms_sender_data WHERE `session` = '$identifier'");
                    foreach ($sql as $result) {
                        return $result->rechnung_ort;
                    }
                    break;

                case 'teilnehmer_anrede':
                    $sql = $wpdb->get_results("SELECT teilnehmer_anrede FROM forms_sender_data WHERE `session` = '$identifier'");
                    foreach ($sql as $result) {
                        return $result->teilnehmer_anrede;
                    }
                    break;
                case 'teilnehmer_vorname':
                    $sql = $wpdb->get_results("SELECT teilnehmer_vorname FROM forms_sender_data WHERE `session` = '$identifier'");
                    foreach ($sql as $result) {
                        return $result->teilnehmer_vorname;
                    }
                    break;
                case 'teilnehmer_nachname':
                    $sql = $wpdb->get_results("SELECT teilnehmer_nachname FROM forms_sender_data WHERE `session` = '$identifier'");
                    foreach ($sql as $result) {
                        return $result->teilnehmer_nachname;
                    }
                    break;
                case 'teilnehmer_email':
                    $sql = $wpdb->get_results("SELECT teilnehmer_email FROM forms_sender_data WHERE `session` = '$identifier'");
                    foreach ($sql as $result) {
                        return $result->teilnehmer_email;
                    }
                    break;
                case 'Handy_Nr':
                    $sql = $wpdb->get_results("SELECT Handy_Nr FROM forms_sender_data WHERE `session` = '$identifier'");
                    foreach ($sql as $result) {
                        return $result->Handy_Nr;
                    }
                    break;
                case 'tn_anrede':
                    $sql = $wpdb->get_results("SELECT tn_anrede FROM forms_sender_data WHERE `session` = '$identifier'");
                    foreach ($sql as $result) {
                        return $result->tn_anrede;
                    }
                    break;
                case 'tn_vorname':
                    $sql = $wpdb->get_results("SELECT tn_vorname FROM forms_sender_data WHERE `session` = '$identifier'");
                    foreach ($sql as $result) {
                        return $result->tn_vorname;
                    }
                    break;
                case 'tn_nachname':
                    $sql = $wpdb->get_results("SELECT tn_nachname FROM forms_sender_data WHERE `session` = '$identifier'");
                    foreach ($sql as $result) {
                        return $result->tn_nachname;
                    }
                    break;
                case 'tn_email':
                    $sql = $wpdb->get_results("SELECT tn_email FROM forms_sender_data WHERE `session` = '$identifier'");
                    foreach ($sql as $result) {
                        return $result->tn_email;
                    }
                    break;
                case 'tn2_anrede':
                    $sql = $wpdb->get_results("SELECT tn2_anrede FROM forms_sender_data WHERE `session` = '$identifier'");
                    foreach ($sql as $result) {
                        return $result->tn2_anrede;
                    }
                    break;
                case 'tn2_vorname':
                    $sql = $wpdb->get_results("SELECT tn2_vorname FROM forms_sender_data WHERE `session` = '$identifier'");
                    foreach ($sql as $result) {
                        return $result->tn2_vorname;
                    }
                    break;
                case 'tn2_nachname':
                    $sql = $wpdb->get_results("SELECT tn2_nachname FROM forms_sender_data WHERE `session` = '$identifier'");
                    foreach ($sql as $result) {
                        return $result->tn2_nachname;
                    }
                    break;
                case 'tn2_email':
                    $sql = $wpdb->get_results("SELECT tn2_email FROM forms_sender_data WHERE `session` = '$identifier'");
                    foreach ($sql as $result) {
                        return $result->tn2_email;
                    }
                    break;
                case 'tn2_Handy_Nr':
                    $sql = $wpdb->get_results("SELECT tn2_Handy_Nr FROM forms_sender_data WHERE `session` = '$identifier'");
                    foreach ($sql as $result) {
                        return $result->tn2_Handy_Nr;
                    }
                    break;

                case 'tn3_anrede':
                    $sql = $wpdb->get_results("SELECT tn3_anrede FROM forms_sender_data WHERE `session` = '$identifier'");
                    foreach ($sql as $result) {
                        return $result->tn3_anrede;
                    }
                    break;
                case 'tn3_vorname':
                    $sql = $wpdb->get_results("SELECT tn3_vorname FROM forms_sender_data WHERE `session` = '$identifier'");
                    foreach ($sql as $result) {
                        return $result->tn3_vorname;
                    }
                    break;
                case 'tn3_nachname':
                    $sql = $wpdb->get_results("SELECT tn3_nachname FROM forms_sender_data WHERE `session` = '$identifier'");
                    foreach ($sql as $result) {
                        return $result->tn3_nachname;
                    }
                    break;
                case 'tn3_email':
                    $sql = $wpdb->get_results("SELECT tn3_email FROM forms_sender_data WHERE `session` = '$identifier'");
                    foreach ($sql as $result) {
                        return $result->tn3_email;
                    }
                    break;
                case 'tn3_Handy_Nr':
                    $sql = $wpdb->get_results("SELECT tn3_Handy_Nr FROM forms_sender_data WHERE `session` = '$identifier'");
                    foreach ($sql as $result) {
                        return $result->tn3_Handy_Nr;
                    }
                    break;

                case 'parkplatz':
                    $sql = $wpdb->get_results("SELECT parkplatz FROM forms_sender_data WHERE `session` = '$identifier'");
                    foreach ($sql as $result) {
                        return $result->parkplatz;
                    }
                    break;

                case 'shuttle':
                    $sql = $wpdb->get_results("SELECT shuttle FROM forms_sender_data WHERE `session` = '$identifier'");
                    foreach ($sql as $result) {
                        return $result->shuttle;
                    }
                    break;

                case 'hotelreservierung':
                    $sql = $wpdb->get_results("SELECT hotelreservierung FROM forms_sender_data WHERE `session` = '$identifier'");
                    foreach ($sql as $result) {
                        return $result->hotelreservierung;
                    }
                    break;

                case 'hotel':
                    $sql = $wpdb->get_results("SELECT hotel FROM forms_sender_data WHERE `session` = '$identifier'");
                    foreach ($sql as $result) {
                        return $result->hotel;
                    }
                    break;

                case 'anreise':
                    $sql = $wpdb->get_results("SELECT anreise FROM forms_sender_data WHERE `session` = '$identifier'");
                    foreach ($sql as $result) {
                        return $result->anreise;
                    }
                    break;

                case 'abreise':
                    $sql = $wpdb->get_results("SELECT abreise FROM forms_sender_data WHERE `session` = '$identifier'");
                    foreach ($sql as $result) {
                        return $result->abreise;
                    }
                    break;

                case 'comments':
                    $sql = $wpdb->get_results("SELECT sonstiges1 FROM forms_sender_data WHERE `session` = '$identifier'");
                    foreach ($sql as $result) {
                        return $result->sonstiges1;
                    }
                    break;
            }

        }
    }

    include(plugin_dir_path(__FILE__) . 'options/options.php');
}
