<?php
/**
 * Template Name: stats
 */
get_header();
?>

<script type="text/javascript"
        src="https://buchungen.gfu.net/wp-content/plugins/gfu-form-receiver-sender/charts/js/fusioncharts.js"></script>
<script type="text/javascript"
        src="https://buchungen.gfu.net/wp-content/plugins/gfu-form-receiver-sender/charts/js/themes/fusioncharts.theme.ocean.js"></script>
<script>

    $(document).ready(function () {
        $(".story .btnShow").click(function () {
            $(this).parent().find(".expandedText").show();
        });
        $(".story .btnHid").click(function () {
            $(this).parent().find(".expandedText").hide();
        });
    });
</script>
<style>
    #sidebar {
        display: none;
    }

    .row {
        margin-right: -15px;
        margin-left: -15px;
        width: 1200px;
    }
    .expandedText {
        display: none;
    }

    .statisticstable {

    }

    .statisticstable td {
        border: 1px solid #ccc;
        padding: 5px;

    }

    td.head {
        background: #cccccc;
        color: #ffffff;
        font-size: 24px;
    }

</style>

<div id="primary1" class="content-area1">
    <main id="main1" class="site-main1" role="main1">
        <?php


        if (session_id() == '' || !isset($_SESSION)) {
            session_start();
        }
        global $wpdb;
        $identifier = session_id();

        //Statistics

        $cookiesyes = $wpdb->get_results("SELECT cookie FROM forms_sender_submitted WHERE cookie = '1'");
        $cookiesacceptance = (count($cookiesyes));

        $cookiesno = $wpdb->get_results("SELECT cookie FROM forms_sender_submitted WHERE cookie = '0'");
        $cookiesnoacceptance = (count($cookiesno));

        $womanyes = $wpdb->get_results("SELECT ap_anrede FROM forms_sender_submitted WHERE ap_anrede = 'Frau'");
        $woman = (count($womanyes));

        $manyes = $wpdb->get_results("SELECT ap_anrede FROM forms_sender_submitted WHERE ap_anrede = 'Herr'");
        $man = (count($manyes));

        //By Title of Seminar
        $resultArray = $wpdb->get_results("SELECT S FROM forms_sender_submitted", ARRAY_N);
        $new_array = array();
        foreach ($resultArray as $key => $title) {
            $new_array[$key] = $title[0];
        }


        $counts = array_count_values($new_array);
        //print_r (($counts));


        $datatitle = [];
        $datacount = [];
        foreach ($counts as $seminartitle => $amount) {
            $whats = $seminartitle;
            $howmanys = $amount;
            $datatitle[] = $whats;
            $datacount[] = $amount;
        }

        //End by Title
        $hotelyes = $wpdb->get_results("SELECT S FROM forms_sender_submitted where hotelreservierung = '1'");
        $hotelcountyes = (count($hotelyes));

        $hotelno = $wpdb->get_results("SELECT S FROM forms_sender_submitted where hotelreservierung = '0'");
        $hotelcountno = (count($hotelno));


        include('/var/www/forms/wp-content/plugins/gfu-form-receiver-sender/charts/fusioncharts.php');
        $pie3dChart = new FusionCharts("pie3d", "ex2", "100%", 400, "chart-1", "json", '{   "chart": {
        "caption": "Cookies Ja vs. Cookies Nein",
        "subcaption": "Aktuell",
        "startingangle": "120",
        "showlabels": "0",
        "showlegend": "1",
        "enablemultislicing": "0",
        "slicingdistance": "15",
        "showpercentvalues": "1",
        "showpercentintooltip": "0",
        "plottooltext": "Insgesamt : $label Total visit : $datavalue",
        "exportEnabled": "1", 
        "theme": "ocean"
    },
    "data": [
        {
            "label": "Cookie NO",
            "value": "' . $cookiesnoacceptance . '"
        },
        {
            "label": "Cookie YES",
            "value": "' . $cookiesacceptance . '"
        },
   
    ]
}');
        $pie3dChart->render();


        $pie3dChart2 = new FusionCharts("pie3d", "ex3", "100%", 400, "chart-2", "json", '{   "chart": {
        "caption": "AP Männlich vs. AP Weiblich (bei cookie=1)",
        "subcaption": "Aktuell",
        "startingangle": "120",
        "showlabels": "0",
        "showlegend": "1",
        "enablemultislicing": "0",
        "slicingdistance": "15",
        "showpercentvalues": "1",
        "showpercentintooltip": "0",
        "plottooltext": "Männlic/Weiblich : $label Total visit : $datavalue",
        "theme": "ocean",
        "exportEnabled": "1"
    },
    "data": [
        {
            "label": "Weiblich",
            "value": "' . $woman . '"
        },
        {
            "label": "Männlich",
            "value": "' . $man . '"
        },
   
    ]
}');
        // Render the chart
        $pie3dChart2->render();


        $pie3dChart3 = new FusionCharts("pie3d", "ex4", "100%", 400, "chart-3", "json", '{   "chart": {
        "caption": "Verteilung auf Seminare (bei cookie=1)",
        "subcaption": "Aktuell",
        "startingangle": "120",
        "showlabels": "0",
        "showlegend": "1",
        "enablemultislicing": "0",
        "slicingdistance": "15",
        "showpercentvalues": "1",
        "showpercentintooltip": "0",
        "plottooltext": "[ Seminartitle : $label ] [ Anzahl Buchungen : $datavalue ]",
        "exportEnabled": "1", 
        "theme": "ocean"
    },
    "data": [
        {
            "label": "'.$datatitle[0].'",
            "value": "'.$datacount[0].'"
        },
        {
            "label": "'.$datatitle[1].'",
            "value": "'.$datacount[1].'"
        },
        
        {
            "label": "'.$datatitle[2].'",
            "value": "'.$datacount[2].'"
        },
         {
            "label": "'.$datatitle[3].'",
            "value": "'.$datacount[3].'"
        },
         {
            "label": "'.$datatitle[4].'",
            "value": "'.$datacount[4].'"
        },
        
         {
            "label": "'.$datatitle[5].'",
            "value": "'.$datacount[5].'"
        },
        
         {
            "label": "'.$datatitle[6].'",
            "value": "'.$datacount[6].'"
        },
        
         {
            "label": "'.$datatitle[7].'",
            "value": "'.$datacount[7].'"
        },
        
         {
            "label": "'.$datatitle[8].'",
            "value": "'.$datacount[8].'"
        },
        
         {
            "label": "'.$datatitle[9].'",
            "value": "'.$datacount[9].'"
        },
        
         {
            "label": "'.$datatitle[10].'",
            "value": "'.$datacount[10].'"
        },
   
    ]
}');


        $pie3dChart3->render();

        $column3dChart4 = new FusionCharts("column3d", "ex5", "100%", 400, "chart-4", "json", '{   "chart": {
        "caption": "Hotel Serviceleistung bezogen auf alle Buchungen",
        "subcaption": "Aktuell",
        "xAxisName": "Hotelbuchung ja/nein",
        "yAxisName": "Anzahl",
        "paletteColors": "#0075c2",
        "valueFontColor": "#ffffff",
        "baseFont": "Helvetica Neue,Arial",
        "captionFontSize": "14",
        "subcaptionFontSize": "14",
        "subcaptionFontBold": "0",
        "placeValuesInside": "1",
        "rotateValues": "1",
        "showShadow": "0",
        "divlineColor": "#999999",
        "divLineDashed": "1",
        "divlineThickness": "1",
        "divLineDashLen": "1",
        "canvasBgColor": "#ffffff",
        "theme": "ocean",
        "exportEnabled": "1" 
    },
    "data": [
        {
            "label": "Hotelservice",
            "value": "'.$hotelcountyes.'"
        },
        {
             "label": "Kein Hotelservice",
              "value": "'.$hotelcountno.'"
        },
   
    ]
}');

        $column3dChart4->render();
        ?>
        <table width="100%" border="1">
            <tr>
                <td width="50%">
                        <div id="chart-1">
                    </div>
                </td>
                <td width="50%">
                      <div id="chart-2"></div>
                </td>
            </tr>
            <td width="50%">
                    <div id="chart-3"></div>
            </td>
            <td width="50%">
                <div id="chart-4"></div>
            </td>
            </tr></table>

        <hr noshade> <hr noshade>
        <h1 style="color:black;">Aktuelle Einzelbuchungen Details</h1>
        <hr noshade> <hr noshade>
        <div style="background: #f2f2f2;">
        <?php
        $rows = $wpdb->get_results("SELECT * FROM forms_sender_submitted;");


        foreach ($rows as $row) {
            $time = strtotime($row->created);
            $mytime = date("d/m/y G:i", $time);
            echo '<div class="story" name="stats"><h4>Buchung Nr.: ' . $row->id . ' vom: ' . $mytime . '</h4>
            <a href="#stats" class="btnShow">[ Open ]</a>
            <table class="statisticstable expandedText" style="background: #000; color: #009900;">
            <a href="#stats" class="btnHid">[ Close ]</a>';
            echo '<tr><td>ID</td><td>' . $row->id . '</td></tr>';
            echo '<tr><td>Created</td><td>' . $mytime . '</td></tr>';
            echo '<tr><td>IP</td><td>' . $row->ipaddress . '</td></tr>';
            echo '<tr><td>Browser</td><td>' . $row->browserinfo . '</td></tr>';
            echo '<tr><td>Session</td><td>' . $row->submission . '</td></tr>';
            echo '<tr><td>Woher gekommen</td><td>' . $row->referrer . '</td></tr>';
            echo '<tr><td>Accept Cookie</td><td>' . $row->cookie . '</td></tr>';
            echo '<tr><td>Urlraw</td><td>' . $row->urlraw . '</td></tr>';
            echo '<tr><td>Parameter: S</td><td>' . $row->S . '</td></tr>';
            echo '<tr><td>Parameter: T</td><td>' . $row->T . '</td></tr>';
            echo '<tr><td>Parameter: G</td><td>' . $row->G . '</td></tr>';
            echo '<tr><td>Parameter: I</td><td>' . $row->I . '</td></tr>';
            echo '<tr><td>Parameter: D</td><td>' . $row->D . '</td></tr>';
            echo '<tr><td>Parameter: A</td><td>' . $row->A . '</td></tr>';
            echo '<tr><td>Parameter: FBP</td><td>' . $row->FBP . '</td></tr>';
            echo '<tr><td>Parameter: Folgeseminar</td><td>' . $row->folgeseminar . '</td></tr>';
            echo '<tr><td>Parameter: Kategorie</td><td>' . $row->cat . '</td></tr>';
            echo '<tr><td>Parameter: kDrei</td><td>' . $row->kDrei . '</td></tr>';
            echo '<tr><td>Parameter: hKnapp</td><td>' . $row->hKnapp . '</td></tr>';
            echo '<tr><td>Parameter: Ort</td><td>' . $row->ort . '</td></tr>';
            echo '<tr><td>Parameter: Zertifikat</td><td>Zertifikatpreis: ' . $row->zerti . ' Euronen</td></tr>';
            echo '<tr><td>Session</td><td>' . $row->session . '</td></tr>';
            echo '<tr><td>Googleid(GA)</td><td>' . $row->googleid . '</td></tr>';
            echo '<tr><td>Firma</td><td>' . $row->firm . '</td></tr>';
            echo '<tr><td>Straße</td><td>' . $row->str . '</td></tr>';
            echo '<tr><td>PLZ</td><td>' . $row->plz . '</td></tr>';
            echo '<tr><td>AP Anrede</td><td>' . $row->ap_anrede . '</td></tr>';
            echo '<tr><td>AP Vorname</td><td>' . $row->ap_vorname . '</td></tr>';
            echo '<tr><td>AP Nachname</td><td>' . $row->ap_nachname . '</td></tr>';
            echo '<tr><td>AP Telefon</td><td>' . $row->telefon . '</td></tr>';
            echo '<tr><td>AP Mail</td><td>' . $row->ap_email . '</td></tr>';
            echo '<tr><td>Alternative Rechnung</td><td>' . $row->altbill . '</td></tr>';
            echo '<tr><td>Alt Rechnung Firma</td><td>' . $row->rechnung_firma . '</td></tr>';
            echo '<tr><td>Alt Rechnung Straße</td><td>' . $row->rechnung_str . '</td></tr>';
            echo '<tr><td>Alt Rechnung PLZ</td><td>' . $row->rechnung_plz . '</td></tr>';
            echo '<tr><td>Alt Rechnung Ort</td><td>' . $row->rechnung_ort . '</td></tr>';
            echo '<tr><td>Teilnehmer 1 Anrede</td><td>' . $row->teilnehmer_anrede . '</td></tr>';
            echo '<tr><td>Teilnehmer 1 Vorname</td><td>' . $row->teilnehmer_vorname . '</td></tr>';
            echo '<tr><td>Teilnehmer 1 Nachname</td><td>' . $row->teilnehmer_nachname . '</td></tr>';
            echo '<tr><td>Teilnehmer 1 Email</td><td>' . $row->teilnehmer_email . '</td></tr>';
            echo '<tr><td>Teilnehmer 1 Handy_Nr</td><td>' . $row->Handy_Nr . '</td></tr>';
            echo '<tr><td>Teilnehmer 2 Anrede</td><td>' . $row->tn2_anrede . '</td></tr>';
            echo '<tr><td>Teilnehmer 2 Vorname</td><td>' . $row->tn2_vorname . '</td></tr>';
            echo '<tr><td>Teilnehmer 2 Nachname</td><td>' . $row->tn2_nachname . '</td></tr>';
            echo '<tr><td>Teilnehmer 2 Email</td><td>' . $row->tn2_email . '</td></tr>';
            echo '<tr><td>Teilnehmer 2 Handy_Nr</td><td>' . $row->tn2_Handy_Nr . '</td></tr>';
            echo '<tr><td>Teilnehmer 3 Anrede</td><td>' . $row->tn3_anrede . '</td></tr>';
            echo '<tr><td>Teilnehmer 3 Vorname</td><td>' . $row->tn3_vorname . '</td></tr>';
            echo '<tr><td>Teilnehmer 3 Nachname</td><td>' . $row->tn3_nachname . '</td></tr>';
            echo '<tr><td>Teilnehmer 3 Email</td><td>' . $row->tn3_email . '</td></tr>';
            echo '<tr><td>Teilnehmer 3 Handy_Nr</td><td>' . $row->tn3_Handy_Nr . '</td></tr>';
            echo '<tr><td>Parkplatz</td><td>' . $row->parkplatz . '</td></tr>';
            echo '<tr><td>Shuttle</td><td>' . $row->shuttle . '</td></tr>';
            echo '<tr><td>Hotelreservierung</td><td>' . $row->hotelreservierung . '</td></tr>';
            echo '<tr><td>Hotel</td><td>' . $row->hotel . '</td></tr>';
            echo '<tr><td>hotel Anreise</td><td>' . $row->anreise . '</td></tr>';
            echo '<tr><td>Hotel Abreise</td><td>' . $row->abreise . '</td></tr>';
            echo '<tr><td>Anmerkungen</td><td>' . $row->sonstiges1 . '</td></tr>';
            echo '</table></div>';
        }
        ?></div>
    </main><!-- .site-main -->
</div><!-- .content-area -->
<?php get_footer(); ?>

